/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Arista implements ConstructorFigura{
    private JComponent figura;
    private int inicioX, inicioY, finX, finY;
    private String nombre;

    public Arista() {
        this.nombre = "";
        this.inicioX = 0;
        this.inicioY = 0;
        this.finX = 0;
        this.finY = 0;
    }

    @Override
    public void construirFigura() {
        
        if (finX > inicioX && finY < inicioY) {
           inicioY = inicioY - finY;
           finX = finX -inicioX ;
           finY = 0;
           inicioX = 0;
           figura = new Figura(new Dimension(finX ,inicioY));
        }
        else if (finX < inicioX && finY < inicioY) {
           inicioX = inicioX - finX;
           inicioY = inicioY - finY;
           finX = 0;
           finY = 0;
           figura = new Figura(new Dimension(inicioX, inicioY));
        }
        else if (finX < inicioX && finY > inicioY) {
           inicioX = inicioX - finX;
           finY = finY - inicioY;
           finX = 0;
           inicioY = 0;
           figura = new Figura(new Dimension(inicioX, finY));
        }
        else  {
           finX = finX - inicioX;
           finY = finY - inicioY;
           inicioX = 0;
           inicioY = 0;
           figura = new Figura(new Dimension(finX, finY));
        }

        JComponent linea = new Linea(inicioX, inicioY, finX, finY, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, Math.abs((finX-inicioX)/2), Math.abs((finY-inicioY)/2), figura.getSize());
        
        figura.add(linea);
        figura.add(nombreTextBox);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "inicioX":
                this.inicioX = (int) parametro;
                break;
            case "inicioY":
                this.inicioY = (int) parametro;
                break;
            case "finX":
                this.finX = (int) parametro;
                break;
            case "finY":
                this.finY = (int) parametro;
                break;
            case "nombre":
                this.nombre = (String) parametro;
                break;
                        
        }
    }
    
}
