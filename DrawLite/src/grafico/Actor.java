/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Actor implements ConstructorFigura{
    private JComponent figura;
    private int x, y; //posicion inicuial de la figura
    private String nombre;//nombre del actor

    public Actor() {
        nombre = "";
    }
    
    @Override
    public void construirFigura() {
		this.x = -25;
        this.y = 5;
        figura = new Figura(new Dimension(50,100));
        JComponent cabeza = new Circulo(false, x+40, y+5, 20, 20, figura.getSize());
        JComponent torzo = new Linea(x+50, y+25, x+50, y+55, figura.getSize());
        JComponent brazos = new Linea(x+35, y+37, x+65, y+37, figura.getSize());
        JComponent pierna1 = new Linea(x+50, y+55, x+65, y+70, figura.getSize());
        JComponent pierna2 = new Linea(x+35, y+70, x+50, y+55, figura.getSize());
        JComponent nombreActor = new TextBox(nombre, x+50-(5*nombre.length()/2), y+85, figura.getSize());
        
        
        JComponent conectoriIzquierda = new Rectangulo(true, 45, 40, 5, 5, figura.getSize());
        JComponent conectoriDerecha = new Rectangulo(true, 0, 40, 5, 5, figura.getSize());
        JComponent conectoriArriba = new Rectangulo(true, 23, 0, 5, 5, figura.getSize());
        JComponent conectoriAbajo = new Rectangulo(true, 23, 95, 5, 5, figura.getSize());
                
        figura.add(cabeza);
        figura.add(torzo);
        figura.add(brazos);
        figura.add(pierna1);
        figura.add(pierna2);
        figura.add(nombreActor);
        
        figura.add(conectoriIzquierda);
        figura.add(conectoriDerecha);
        figura.add(conectoriArriba);
        figura.add(conectoriAbajo);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "nombre":
                this.nombre = (String) parametro;
                break;
        }
    }
    
}
