/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Circulo extends JComponent{
	
	private final boolean lleno;
    int x,y,width,height;
	
	public Circulo(boolean lleno, int x, int y, int width,  int height, Dimension size) {
        super();
        this.lleno = lleno;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        super.setSize(size);
    }
	
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if(lleno)
            g.fillOval(x,y,width,height);
        else
            g.drawOval(x,y,width,height);
    }
}