/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author SERGIO MALDONADO
 */
public class Rombo extends JComponent{
	
	private final boolean lleno;
	private int x,y,ancho,alto;

	public Rombo(boolean lleno, int x, int y, int ancho, int alto, Dimension size) {
		this.lleno = lleno;
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
                super.setSize(size);
	}
	//	p2			*
	//			 *	   *
	//	p1	    *		*  p3
	//			  *   *
	//	p4			*
	@Override
    public void paint(Graphics g) {
		int [] posX = new int[4];//array de los valores x de los 4 puntos
		int [] posY = new int[4];//array de los valores y de los 4 puntos
		posX[0] = x;
		posY[0] = y;
		posX[1] = x+ancho/2;
		posY[1] = y-alto/2;
		posX[2] = x+ancho;
		posY[2] = y;
		posX[3] = x+ancho/2;
		posY[3] = y+alto/2;
                super.paint(g);
        if(lleno)
            g.fillPolygon(posX, posY, 4);
        else
            g.drawPolygon(posX, posY, 4);
    }
	
}
