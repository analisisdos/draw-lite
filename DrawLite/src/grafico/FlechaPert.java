/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class FlechaPert implements ConstructorFigura{
    private JComponent figura;
    private int inicioX, inicioY, finX, finY, tipo;
    private boolean llena;
    private String nombre;

    public FlechaPert() {
        this.inicioX = 0;
        this.inicioY = 0;
        this.finX = 0;
        this.finY = 0;
        this.tipo = 0;
        this.llena = false;
        this.nombre = "";
    }
    

    @Override
    public void construirFigura() {
        figura = new Figura(new Dimension(finX > inicioX ? finX + 20: inicioX + 20, finY > inicioY ? finY + 20: inicioY + 20));
        
        switch(tipo){
            case 1: 
                finX -= 10;
                break;
            case 2: 
                finX += 10;
                break;
            case 3: 
                finY += 10;
                break;
            case 4: 
                finY -= 10;
                break;
        }
        
        JComponent punta = new Triangulo(llena, finX, finY, tipo, figura.getSize());
        
        JComponent linea = new Linea(inicioX, inicioY, finX, finY, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, Math.abs((finX-inicioX)/2), Math.abs((finY-inicioY)/2), figura.getSize());
        
        figura.add(punta);
        figura.add(linea);
        figura.add(nombreTextBox);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "inicioX":
                this.inicioX = (int) parametro;
                break;
            case "inicioY":
                this.inicioY = (int) parametro;
                break;
            case "finX":
                this.finX = (int) parametro;
                break;
            case "finY":
                this.finY = (int) parametro;
                break;
            case "llena":
                this.llena = (boolean) parametro;
                break;
            case "tipo":
                this.tipo = (int) parametro;
                break;
            case "nombre":
                this.nombre = (String) parametro;
                break;
                        
        }
    }
    
}