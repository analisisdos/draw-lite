/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class CirculoDobleInicial implements ConstructorFigura{
    JComponent figura;
    private String nombre;
    private int x, y, diametro;
    
    public CirculoDobleInicial() {
        this.nombre = "";
    }
    
    
    
    @Override
    public void construirFigura() {
		this.x = 0;
        this.y = 0;
        this.diametro = 50;
        
        figura = new Figura(new Dimension(diametro + 22, diametro + 12));
        
        x += 15;
        y += 5;
        
        JComponent circulo = new Circulo(false, x, y, diametro, diametro, figura.getSize());
        JComponent circulo2 = new Circulo(false, x-5, y-5, diametro+10, diametro+10, figura.getSize());
        
        
        JComponent triangulo = new Triangulo(true, x - 15 , y + (diametro/2), 1, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, x + (diametro/2) - (7*nombre.length()/2), y + (diametro/2) + 2, figura.getSize());
        
        JComponent conectoriIzquierda = new Rectangulo(true, diametro+15, (diametro/2)+3, 5, 5, figura.getSize());
        JComponent conectoriArriba = new Rectangulo(true, (diametro/2) + 13, 0, 5, 5, figura.getSize());
        JComponent conectoriAbajo = new Rectangulo(true, (diametro/2) + 13, diametro + 5, 5, 5, figura.getSize());
        
        figura.add(circulo);
        figura.add(circulo2);
        figura.add(triangulo);
        figura.add(nombreTextBox);
        figura.add(conectoriIzquierda);
        figura.add(conectoriArriba);
        figura.add(conectoriAbajo);
        
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "nombre":
                this.nombre = (String) parametro;
                break;                
        }
    }
    
}