/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author Sergio Guzmán
 */
public class FlechaPunteada implements ConstructorFigura{
    private JComponent figura;
    private int inicioX, inicioY, finX, finY, tipo;
    private boolean llena;
    private String nombre;
    
    public FlechaPunteada() {
        this.llena = false;
        this.nombre = "";
        
        this.inicioX = 0;
        this.inicioY = 0;
        this.finX = 0;
        this.finY = 0;
        this.tipo = 0;
    }
    
    
    @Override
    public void construirFigura() {
        
        if (finX > inicioX && finY < inicioY) {
           inicioY = inicioY - finY;
           finX = finX -inicioX - 10;
           finY = 5;
           inicioX = 0;
           figura = new Figura(new Dimension(finX +10, inicioY -5));
        }
        else if (finX < inicioX && finY < inicioY) {
           inicioX = inicioX - finX;
           inicioY = inicioY - finY;
           finX = 10;
           finY = 5;
           figura = new Figura(new Dimension(inicioX, inicioY));
        }
        else if (finX < inicioX && finY > inicioY) {
           inicioX = inicioX - finX;
           finY = finY - inicioY -5 ;
           finX = 10;
           inicioY = 0;
           figura = new Figura(new Dimension(inicioX, finY +5));
        }
        else  {
           finX = finX - inicioX - 10;
           finY = finY - inicioY - 5;
           inicioX = 0;
           inicioY = 0;
           figura = new Figura(new Dimension(finX + 10, finY +5));
        }
        
        JComponent punta = new Triangulo(llena, finX, finY, tipo, figura.getSize());
        
        JComponent lineaPunteada = new LineaPunteada(inicioX, inicioY, finX, finY, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, Math.abs((finX-inicioX)/2), Math.abs((finY-inicioY)/2), figura.getSize());
        
        figura.add(punta);
        figura.add(lineaPunteada);
        figura.add(nombreTextBox);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "inicioX":
                this.inicioX = (int) parametro;
                break;
            case "inicioY":
                this.inicioY = (int) parametro;
                break;
            case "finX":
                this.finX = (int) parametro;
                break;
            case "finY":
                this.finY = (int) parametro;
                break;
            case "llena":
                this.llena = (boolean) parametro;
                break;
            case "tipo":
                this.tipo = (int) parametro;
                break;
            case "nombre":
                this.nombre = (String) parametro;
                break;
                        
        }
    }
    
}
