/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Triangulo extends JComponent{
	//Constantes utilizadas para la direccion del triangulo
	public static final int DERECHA = 1;
	public static final int IZQUIERDA = 2;
	public static final int ARRIBA = 3;
	public static final int ABAJO = 4;
	
	private final int dimension = 10; //Constante para hacer el calculo del tamano de la base y la altura 
	private final boolean lleno; //parametros si es lleno o vacio
	int x,y; // coordenadas del centro de la base del triangulo (Donde se conectara una posible linea)
	int tipo; //paramtro que indica la direccion del triangulo

	public Triangulo(boolean lleno, int x, int y, int tipo,Dimension size) {
		this.lleno = lleno;
		this.x = x;
		this.y = y;
		this.tipo = tipo;
		super.setSize(size);
	}
	/**
	 * Realiza los calculos de los tres puntos, sus coordenadas x-y, y el segun el tipo de triangulo
	 * Los calculos de los tres puntos los hace en base a la direccion y a la constante dimension
	 * Arrays de puntos van en orden x,y
	 * @param g Objeto Graphics
	 */
    @Override
    public void paint(Graphics g) {
		int [] posX = new int[3];//array de los valores x de los tres puntos del triangulo
		int [] posY = new int[3];//array de los valores y de los tres puntos del triangulo
		if(tipo == 1){ //Direccion derecha
			posX[0] = x;
			posY[0] = y-(dimension/2);
			
			posX[1] = x;
			posY[1] = y+(dimension/2);
			
			posX[2] = x+dimension;
			posY[2] = y;
		}
		if(tipo == 2){//Direccion izquierda
			posX[0] = x;
			posY[0] = y-(dimension/2);
			
			posX[1] = x;
			posY[1] = y+(dimension/2);
			
			posX[2] = x-dimension;
			posY[2] = y;
		}
		if(tipo == 3){//Direccion hacia arriba
			posX[0] = x-(dimension/2);
			posY[0] = y;
			
			posX[1] = x+(dimension/2);
			posY[1] = y;
			
			posX[2] = x;
			posY[2] = y-dimension;
		}
		if(tipo == 4){//Direccion hacia abajo
			posX[0] = x-(dimension/2);
			posY[0] = y;
			
			posX[1] = x+(dimension/2);
			posY[1] = y;
			
			posX[2] = x;
			posY[2] = y+dimension;
		}
        super.paint(g);
        if(lleno)
            g.fillPolygon(posX, posY, 3);
        else
            g.drawPolygon(posX, posY, 3);
    }
    
}