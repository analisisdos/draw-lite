/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public interface ConstructorFigura {
   public void construirFigura();
   public JComponent getFigura();
   public void setParametro(String id, Object parametro);
}
