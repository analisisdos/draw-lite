/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class CirculoSimple implements ConstructorFigura{
    JComponent figura;
    private String nombre;
    private int x, y, diametro;
    
    public CirculoSimple() {
        this.nombre = "";
        
        this.diametro = 50;
    }
    
    
    
    @Override
    public void construirFigura() {
		this.x = 0;
        this.y = 0;
        figura = new Figura(new Dimension(diametro, diametro));
        
//        x += 5;
//        y += 5;
        
        JComponent circulo = new Circulo(false, x, y, diametro, diametro, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, x + (diametro/2) - (7*nombre.length()/2), y + (diametro/2) + 2, figura.getSize());
        
        JComponent conectoriIzquierda = new Rectangulo(true, diametro-5, (diametro/2) - 3, 5, 5, figura.getSize());
        JComponent conectoriDerecha = new Rectangulo(true, 0, (diametro/2) - 3, 5, 5, figura.getSize());
        JComponent conectoriArriba = new Rectangulo(true, (diametro/2) - 3, 0, 5, 5, figura.getSize());
        JComponent conectoriAbajo = new Rectangulo(true, (diametro/2) - 3, diametro-5, 5, 5, figura.getSize());
        
        
        figura.add(circulo);
        figura.add(nombreTextBox);
        figura.add(conectoriIzquierda);
        figura.add(conectoriDerecha);
        figura.add(conectoriArriba);
        figura.add(conectoriAbajo);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "nombre":
                this.nombre = (String) parametro;
                break;
        }
    }
    
}