/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Director {
	private ConstructorFigura constructor;
	private JComponent figura;
    public Director() {
        
    }

    public ConstructorFigura getConstructor() {
        return constructor;
    }

    public void setConstructor(ConstructorFigura constructor) {
        this.constructor = constructor;
    }
    
    public void setParametro(String id, Object parametro){
        this.constructor.setParametro(id, parametro);
    }
    public void construir(){
        constructor.construirFigura();
    } 
   
    public JComponent getFigura(){
        return constructor.getFigura();
    }
    
}