/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class RectanguloTextBox implements ConstructorFigura{
    private JComponent figura;
    private int x, y, alto, ancho;
    private String nombre;

    public RectanguloTextBox() {
        this.nombre = "";
        this.alto = 50;
        this.ancho = 100;
    }
    
    
    
    @Override
    public void construirFigura() {
		this.x = 0;
        this.y = 0;
        ancho = nombre.length() * 8;
        
        figura = new Figura(new Dimension(ancho + 1, alto + 1));
        
        JComponent rectangulo = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
        
        JComponent nombreTextBox = new TextBox(nombre, x + (ancho/2) - (6*nombre.length()/2), y + (alto/2) + 2, figura.getSize());
        
        JComponent conectoriIzquierda = new Rectangulo(true, ancho - 5, (alto)/2 - 3, 5, 5, figura.getSize());
        JComponent conectoriDerecha = new Rectangulo(true, 0, (alto)/2 - 3, 5, 5, figura.getSize());
        JComponent conectoriArriba = new Rectangulo(true, (ancho)/2 -2 , 0, 5, 5, figura.getSize());
        JComponent conectoriAbajo = new Rectangulo(true, (ancho)/2 - 2, alto-5, 5, 5, figura.getSize());
        
        figura.add(rectangulo);
        figura.add(nombreTextBox);
        figura.add(conectoriIzquierda);
        figura.add(conectoriDerecha);
        figura.add(conectoriArriba);
        figura.add(conectoriAbajo);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "nombre":
                this.nombre = (String) parametro;
                break;
        }
    }
}
