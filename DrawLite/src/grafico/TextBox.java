/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class TextBox  extends JComponent{
	
	private String texto;//Texto a mostrar
	private int x,y;//Coordenadas del texto

	public TextBox(String texto, int x, int y,Dimension size) {
		this.texto = texto;
		this.x = x;
		this.y = y;
		super.setSize(size);
	}

    @Override
    public void paint(Graphics g) {
		super.paint(g);
        g.drawString(texto, x, y);
    }

    
}