/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class RomboTextBox implements ConstructorFigura{
    private JComponent figura;
    private String nombre;
    private int x, y, dimension;

    public RomboTextBox() {
        this.nombre = "";
        this.dimension = 0;
    }
    
    @Override
    public void construirFigura() {
		this.x = 0;
        this.y = 0;
        dimension = nombre.length() * 11;
        y += dimension/2;
        figura = new Figura(new Dimension(dimension + 1, dimension + 1));
        
        JComponent rombo = new Rombo(false, x, y, dimension, dimension, figura.getSize());
        JComponent nombreText = new TextBox(nombre, x + (dimension/2) - (11*nombre.length()/3), y +3, figura.getSize());
        
        figura.add(rombo);
        figura.add(nombreText);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "nombre":
                this.nombre = (String) parametro;
                break;
        }
    }
}
