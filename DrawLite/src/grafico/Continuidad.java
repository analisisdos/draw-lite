/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class Continuidad implements ConstructorFigura{
    private JComponent figura;
    private int x, y;

    public Continuidad() {
       this.x = 0;
       this.y = 2;
    }
    
    @Override
    public void construirFigura() {
        figura = new Figura(new Dimension(55, 15));
        
        JComponent circulo1 = new Circulo(true, x, y, 10, 10, figura.getSize());
        JComponent circulo2 = new Circulo(true, x + 15, y, 10, 10, figura.getSize());
        JComponent circulo3 = new Circulo(true, x + 30, y, 10, 10, figura.getSize());
        
        figura.add(circulo1);
        figura.add(circulo2);
        figura.add(circulo3);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        
    }
    
}
