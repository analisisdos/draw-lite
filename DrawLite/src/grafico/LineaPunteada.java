/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class LineaPunteada extends JComponent{
    int x1,y1,x2,y2;

	public LineaPunteada(int x1, int y1, int x2, int y2,Dimension size) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		super.setSize(size);
	}

    @Override
    public void paint(Graphics g) {
        Graphics2D g2D = (Graphics2D) g;
        Stroke pincelPunteado = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] {5}, 0);
        g2D.setStroke(pincelPunteado);
        g.drawLine(x1, y1, x2, y2);
    }
    
}
