/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafico;

import java.awt.Dimension;
import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class RectanguloPert implements ConstructorFigura{
    private JComponent figura;
    private int x, y, alto, ancho;
    private String[] texto;

    public RectanguloPert() {
        this.texto = new String[6];
        this.alto = 20;
        this.ancho = 40;
    }
    
    
    /*
            -------------------
            |  1  |  2  |  3  |
            -------------------
            |  4  |  5  |  6  |
            -------------------
    */
	@Override
    public void construirFigura() {
		this.x = 5;
        this.y = 5;
        figura = new Figura(new Dimension(131, 51));
        int textoX, textoY;     
        textoX = x + ancho/2;
        textoY = y + alto/2 + 2;
        //caja1
            JComponent caja1 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto1 = new TextBox(texto[0], textoX - (6*texto[0].length()/2), textoY, figura.getSize());
            x += ancho;
            textoX += ancho;
        //caja2
            JComponent caja2 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto2 = new TextBox(texto[1], textoX - (6*texto[1].length()/2), textoY, figura.getSize());
            x += ancho;
            textoX += ancho;
        //caja3
            JComponent caja3 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto3 = new TextBox(texto[2], textoX - (6*texto[2].length()/2), textoY, figura.getSize());
            x -= 2 * ancho;
            textoX -= 2 * ancho;
            y += alto;
            textoY += alto;
        //caja4
            JComponent caja4 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto4 = new TextBox(texto[3], textoX - (6*texto[3].length()/2), textoY, figura.getSize());
            x += ancho;
            textoX += ancho;
        //caja5
            JComponent caja5 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto5 = new TextBox(texto[4], textoX - (6*texto[4].length()/2), textoY, figura.getSize());
            x += ancho;
            textoX += ancho;
        //caja6
            JComponent caja6 = new Rectangulo(false, x, y, ancho, alto, figura.getSize());
            JComponent texto6 = new TextBox(texto[5], textoX - (6*texto[5].length()/2), textoY, figura.getSize());
            
        JComponent conectoriIzquierda = new Rectangulo(true, 131 -5, (51/2) -2, 5, 5, figura.getSize());
        JComponent conectoriDerecha = new Rectangulo(true, 0, (51/2) -2, 5, 5, figura.getSize());
            
         figura.add(caja1);
         figura.add(caja2);
         figura.add(caja3);
         figura.add(caja4);
         figura.add(caja5);
         figura.add(caja6);
         
         figura.add(texto1);
         figura.add(texto2);
         figura.add(texto3);
         figura.add(texto4);
         figura.add(texto5);
         figura.add(texto6);
         
         
        figura.add(conectoriIzquierda);
        figura.add(conectoriDerecha);
    }

    @Override
    public JComponent getFigura() {
        if (figura!=null) {
            return this.figura;
        }
        else return null;
    }

    @Override
    public void setParametro(String id, Object parametro) {
        switch(id){
            case "x":
                this.x = (int) parametro;
                break;
            case "y":
                this.y = (int) parametro;
                break;
            case "alto":
                this.alto = (int) parametro;
                break;
            case "ancho":
                this.ancho = (int) parametro;
                break;
            case "texto":
                this.texto = (String[]) parametro;
                break;
        }

    }
}
