/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

import Operativo.Diagrama;

/**
 *
 * @author Jonathan
 */
public interface IODiagrama {
    public void guardar();
    public void abrir();
    public void setDiagrama(Diagrama diagram);
    public Diagrama getDiagrama();
}
