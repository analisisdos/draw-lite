/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

/**
 *
 * @author Jonathan
 */
public class FabricaIODiagrama implements NuevoIODiagrama{


    @Override
    public FabricaConstructorIODiagrama createDiagramFactoryBuilder(String type) {
        switch(type){
            case "UML":
                return new FabricaConstructorIODiagrama(type);
            case "PERT":
                return new FabricaConstructorIODiagrama(type);
            case "AFD":
                return new FabricaConstructorIODiagrama(type);
            case "RNA":
                return new FabricaConstructorIODiagrama(type);
            default:
                return null;
        }
    }
}
