/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

import exceptions.WrongDiagramType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jonathan
 */
public class DirectorIODiagrama {
    protected ConstructorIODiagrama builder;
    protected NuevoIODiagrama creator;

    public DirectorIODiagrama() {
    }

    public ConstructorIODiagrama getBuilder() {
        return builder;
    }

    public NuevoIODiagrama getCreator() {
        return creator;
    }
    
    public IODiagrama getSaveDiagram(){
        if (builder != null) {
            return this.builder.getSaveDiagram();
        }else return null;
    }

    public void setBuilder(ConstructorIODiagrama builder) {
        this.builder = builder;
    }

    public void setCreator(NuevoIODiagrama creator) {
        this.creator = creator;
    }
    
    public void setSaveDiagram(IODiagrama saveDiagram){
        if (this.builder != null) {
            this.builder.setSaveDiagram(saveDiagram);
        }
    }
    
    public void save(String Type){
        if (this.builder != null) {
            try {
                this.builder.save(Type);
            } catch (WrongDiagramType ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
