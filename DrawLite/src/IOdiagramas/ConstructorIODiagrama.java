/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

import exceptions.WrongDiagramType;

/**
 *
 * @author Jonathan
 */
public interface ConstructorIODiagrama {
    public IODiagrama getSaveDiagram();
    public void save(String type) throws  WrongDiagramType;
    public void setSaveDiagram(IODiagrama saveDiagram);
}
