/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

import exceptions.WrongDiagramType;

/**
 *
 * @author Jonathan
 */
public class FabricaConstructorIODiagrama implements ConstructorIODiagrama{
    protected IODiagrama saveDiagram;
    protected String saveDiagramType;

    public FabricaConstructorIODiagrama(String saveDiagramType) {
        this.saveDiagramType = saveDiagramType;
    }
        
    @Override
    public IODiagrama getSaveDiagram() {
        return this.saveDiagram;
    }

    @Override
    public void save(String type)throws WrongDiagramType{
        if (this.saveDiagramType.equals(type)) {
            switch(this.saveDiagramType){
                case "UML":
                    ((IOUMLDiagrama) this.saveDiagram).guardar();
                case "PERT":
                    ((IOPERTDiagrama) this.saveDiagram).guardar();
                case "AFD":
                    ((IOAFDDiagrama) this.saveDiagram).guardar();
                case "RNA":
                    ((IORNADiagrama) this.saveDiagram).guardar();
            }
        }else throw new WrongDiagramType("La fabrica es de tipo " + this.saveDiagramType + " y al constructor es de tipo " + type + " se esperaba un contructor de tipo " + this.saveDiagramType);
    }

    @Override
    public void setSaveDiagram(IODiagrama saveDiagram) {
        this.saveDiagram = saveDiagram;
    }
    
}
