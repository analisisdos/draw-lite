/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IOdiagramas;

import UML.DiagramaUML;

/**
 *
 * @author Jonathan
 */
public class prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DirectorIODiagrama director = new DirectorIODiagrama();
        director.setBuilder(new FabricaIODiagrama().createDiagramFactoryBuilder("UML"));
        
        IODiagrama saveUMLDiagram = new IOUMLDiagrama();
        //saveUMLDiagram.setDiagrama(new DiagramaUML());
        
        director.setSaveDiagram(saveUMLDiagram);
        
        director.save("PERT");
    }
    
}
