/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newdiagram;

import automatas.AFD;
import automatas.Automata;
import Operativo.Diagrama;
import automatas.FabricaAutomata;
import automatas.FactoryAutomata;
import PERT.PERT;
import RNA.RNA;
import UML.DiagramaUML;
import java.util.ArrayList;

/**
 *
 * @author RM
 */
public class DiagramFactory implements NewDiagram{

    @Override
    public Diagrama newDiagram(String type) {
        switch(type){
           // case "UML": return new DiagramaUML();
            case "PERT": return new PERT();
            case "AFD":
                ArrayList<String> alphabeto = new ArrayList<String>();
                FactoryAutomata fa = new FabricaAutomata();
                return fa.crearAutomata(0, alphabeto);
            case "RNA": return new RNA();
            default: return null;
        }
    }
    
}
