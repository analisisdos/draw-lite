/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 *
 * @author SERGIO MALDONADO
 */
public abstract class DecoradorObjeto extends Componente{
    private Componente componete;
    public void setComponente(Componente componente){
        this.componete=componente;
    }
    public Componente getComponente(){
        return componete;
    }
}
