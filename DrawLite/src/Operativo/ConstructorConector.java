/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 *
 * @author roberto
 */
public interface ConstructorConector {
  
    public ComponenteDiagrama construirConector(String nombre);

    public Object getPropiedad(String propiedad);

    public void setPropiedad(String propiedad, Object valor);

    public void setFin(ComponenteDiagrama fin);

    public ComponenteDiagrama getInicio();

    public void setInicio(ComponenteDiagrama inicio);

    public ComponenteDiagrama getFin();

}