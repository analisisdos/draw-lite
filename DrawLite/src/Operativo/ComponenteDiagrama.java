/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

import grafico.ConstructorFigura;
import grafico.Figura;
import javax.swing.JComponent;

/**
 *
 * @author roberto
 */
public abstract class ComponenteDiagrama implements ComponenteSeleccionable {

  protected JComponent figura;
  
  protected ConstructorFigura constructor;

    
  public JComponent getFigura() {
      return figura;
  }

  public void setFigura(JComponent figura) {
      this.figura=figura;
  }
  
  public ConstructorFigura getConstructor(){
      return constructor;
  }
  
  public void setConstructor(ConstructorFigura constructor){
      this.constructor = constructor;
  }

  public abstract void setPropiedad(String propiedad, Object valor);

  public abstract Object getPropiedad(String propiedad);

  public abstract String getNombre();

  public abstract void setNombre(String nombre);
  
  public abstract JComponent reconstruirFigura();

}
