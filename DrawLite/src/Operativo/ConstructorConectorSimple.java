/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 *
 * @author roberto
 */
public class ConstructorConectorSimple implements ConstructorConector{
    
    private ComponenteDiagrama inicio;
    private ComponenteDiagrama fin;
    @Override
    public ComponenteDiagrama construirConector(String nombre) {
        return new ConectorSimple(inicio, fin);
    }

    @Override
    public Object getPropiedad(String propiedad) {
        switch(propiedad){
            case "inicio": return inicio;
            case "fin" : return fin;
        }
        return null;
    }

    @Override
    public void setPropiedad(String propiedad, Object valor) {
        switch(propiedad){
            case "inicio": inicio = (Componente) valor;
            case "fin" : fin = (Componente)valor;
        }
    }

    @Override
    public void setFin(ComponenteDiagrama fin) {
        this.fin=fin;
    }

    @Override
    public ComponenteDiagrama getInicio() {
        return inicio;
    }

    @Override
    public void setInicio(ComponenteDiagrama inicio) {
        this.inicio=inicio;
    }

    @Override
    public ComponenteDiagrama getFin() {
        return fin;
    }
    
}
