/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

import grafico.Figura;
import javax.swing.JComponent;

/**
 *Clase abstracta de donde heredan los objetos contenedores en los diagramas
 * <br>Contienen una instancia de Figura para graficar
 * @author SERGIO MALDONADO
 */
public abstract class Componente extends ComponenteDiagrama implements ComponenteSeleccionable{
    @Override
    public boolean validarPosicion(int x, int y){
        return x > figura.getX() && x< figura.getX()+figura.getWidth()
                && y > figura.getY() && y < figura.getY()+figura.getHeight();
        
    }
    @Override
    public void setFigura(JComponent figura){
        this.figura=figura;
    }
    @Override
    public JComponent getFigura(){
        return figura;
    }
    
        @Override
    public boolean esMovible() {
        return true;
    }
    
    @Override
    public boolean esConectable(){
        return true;
    }
    
    @Override
    public int getSector(int x, int y){
        double pendiente1 = (double) figura.getHeight()/figura.getWidth();
        double pendiente2 = -pendiente1;
        if(y >= Math.round(x*pendiente1)
                && y < Math.round(x*pendiente2)+figura.getHeight())
            return IZQUIERDA;
        else if(y >= Math.round(x*pendiente1)
                && y > Math.round(x*pendiente2)+figura.getHeight())
            return ABAJO;
        else if(y <= Math.round(x*pendiente2)+figura.getHeight()
                && y < Math.round(x*pendiente1))
            return ARRIBA;
        else if(y >= Math.round(x*pendiente2)+figura.getHeight()
               && y < Math.round(x*pendiente1))
            return DERECHA;
        else
            return NULL;
    }
}