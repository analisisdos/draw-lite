/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

import grafico.Figura;

/**
 *
 * @author Sergio Guzmán
 */
public abstract class Conector extends ComponenteDiagrama implements ComponenteSeleccionable{
    private String nombre;
    private ComponenteDiagrama inicio;
    private ComponenteDiagrama fin;
    public Conector(ComponenteDiagrama inicio,ComponenteDiagrama fin){
            this.inicio = inicio;
            this.fin = fin;
    }
    
    public ComponenteDiagrama getInicio() {
        return inicio;
    }

    public ComponenteDiagrama getFin() {
        return fin;
    }

    public void setInicio(ComponenteDiagrama inicio) {
        this.inicio = inicio;
    }

    public void setFin(ComponenteDiagrama fin) {
        this.fin = fin;
    }
    
    @Override
    public boolean validarPosicion(int x, int y) {
        if(x > figura.getX() && x < figura.getX()+figura.getWidth()
                && y > figura.getY() && y < figura.getY()+figura.getHeight()){
            boolean izquierda=true, arriba=true;
            if(inicio.getFigura().getX()>fin.getFigura().getX())
                izquierda=false;
            if(inicio.getFigura().getY()>fin.getFigura().getY())
                arriba=false;
            int xLocal = x - figura.getX();
            int yLocal = y - figura.getY();
            int yi,yf;
            if(izquierda&&arriba)
            {
                yi=0;
                yf=figura.getHeight();
            }
            else{
                yf=0;
                yi=figura.getHeight();
            }
            double pendiente = (double)(yf-yi)/figura.getWidth();
            return (yLocal > (pendiente*xLocal)-5+yi && yLocal < (pendiente*xLocal)+5+yi);
        }
        return false;
    }

    @Override
    public boolean esMovible() {
        return false;
    }
    @Override
    public int getSector(int x, int y){
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    @Override
    public boolean esConectable() {
        return false;
    }
    @Override
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    @Override
    public String getNombre(){
        return nombre;
    }
}
