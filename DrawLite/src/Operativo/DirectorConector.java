/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 *
 * @author roberto
 */


public class DirectorConector {

    private ConstructorConector constructor;

    
    public void setPropiedad(String propiedad, Object valor) {
        constructor.setPropiedad(propiedad, valor);
    }

    /**
     * Metodo para la generacion de conectores
     * <br> La primera llamada ingresa Inicio en el Constructor
     * <br> La segunda llamada ingresa Fin al constructor y llama al metodo construir dentro del constructor
     * @param inicio Componente de inicio
     * @param fin Componente de fin
     * @return Un conector si ha sido posible su creacion
     */
    public ComponenteDiagrama construirConector(ComponenteDiagrama inicio, ComponenteDiagrama fin) {
        if(inicio.esConectable() && fin.esConectable()){
            constructor.setInicio(inicio);
            constructor.setFin(fin);

            return constructor.construirConector("");
        }
        return null;
    }

    public Object getPropiedad(String propiedad) {
        return constructor.getPropiedad(propiedad);
    }

    public void setConstructorConector(ConstructorConector constructor) {
        this.constructor=constructor;
    }

    public ConstructorConector getConstructorConector() {
        return constructor;
    }

    public DirectorConector() {
    }

    public DirectorConector(ConstructorConector constructor) {
        this.constructor=constructor;
    }

}