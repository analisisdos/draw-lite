/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

import javax.swing.JComponent;

/**
 * Objeto funcional básico para diagramas
 * @author SERGIO MALDONADO
 */
public class Objeto extends Componente{
    private String nombre;
    public Objeto(String nombre){
        this.nombre = nombre;
    }

    @Override
    public String getNombre() {
        return nombre;
    }


    @Override
    public void setNombre(String nombre) {
        this.nombre=nombre;
    }


    @Override
    public Object getPropiedad(String nombre) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPropiedad(String nombre, Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JComponent reconstruirFigura() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
