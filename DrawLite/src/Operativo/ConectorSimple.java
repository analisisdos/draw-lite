/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

import javax.swing.JComponent;

/**
 *
 * @author Sergio Guzmán
 */
public class ConectorSimple extends Conector{

    public ConectorSimple(ComponenteDiagrama inicio, ComponenteDiagrama fin){
        super(inicio,fin);	
    }

    @Override
    public Object getPropiedad(String nombre) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPropiedad(String nombre, Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public JComponent reconstruirFigura() {
		JComponent figuraSalida;
		constructor.setParametro("nombre",this.getNombre());
		constructor.construirFigura();
		figuraSalida=figura;
        figura=constructor.getFigura();
        figura.setLocation(figuraSalida.getLocation());
        return figuraSalida;
	}
}
