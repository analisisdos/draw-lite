/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 *
 * @author SERGIO MALDONADO
 */
import grafico.ConstructorFigura;
import javax.swing.JComponent;
import javax.swing.JPanel;

public interface Diagrama{

    public void actualizarDiagrama();

    public void crearComponente(Object parametros);

    public void seleccionarComponte(int x, int y);

    public ComponenteDiagrama obtenerSeleccion();

    public void removerSeleccion();

    public void conectar(ComponenteDiagrama componentes);

    public void moverSeleccion(int x, int y);

    public Object getPropiedadSeleccion(String id);

    public void setPropiedadSeleccion(String id, Object valor);
    
    public void setConstructorFigura(ConstructorFigura constructor);
    
    public ConstructorFigura getConstructorFigura();
    
    public JComponent getLienzo();
    
    public void setPropiedadDiagrama(String id, Object valor);
    
    public Object getPropiedadDiagrama(String id);
    
    public ComponenteDiagrama obtenerComponente(int x, int y);
    
    public JComponent getBarraPropiedades();
    
    public void notificarCambio(JComponent barraPropiedades);
}
