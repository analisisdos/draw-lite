/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operativo;

/**
 * Interfaz para los objetos que puedan ser seleccionados dentro del lienzo de un diagrama
 * @author roberto
 */
public interface ComponenteSeleccionable {

    public static int ARRIBA =1;
    public static int ABAJO = 2;
    public static int DERECHA = 3;
    public static int IZQUIERDA = 4;
    public static int NULL = 0;
    /**
     * Función que permite determinar si una coordenada, esta dentro del area de un objeto de diagrama
     * @param x Coordenada en X del lienzo (Panel)
     * @param y Coordenada en Y del lienzo (Panel)
     * @return True si esta contenida - False si no esta contenida
     */
    public boolean validarPosicion(int x, int y);

    /**
     * Funcion que retorna si es posible cambiar la ubicación de forma manual (externa)
     * @return True si es posible - False si no lo es
     */
    public boolean esMovible();
    
    public int getSector(int x, int y);
    
    public boolean esConectable();
    
}
