/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.Conector;
import Operativo.ConstructorConector;

/**
 *Clase que se encarga de la construcción de Conectores UML
 * <br>(Conectores con restricción de conexión)
 * @author roberto
 */
public class ConstructorConectorUML implements ConstructorConector {

    private TipoRelacion tipoRelacion;
    private ComponenteDiagrama inicio, fin;

    public ConstructorConectorUML() {
    }

    public void setTipoRelacion(TipoRelacion tipo) {
        this.tipoRelacion=tipo;
    }

    @Override
    public ComponenteDiagrama construirConector(String nombre) {
        if(inicio==null || fin==null)
            return null;
        if(tipoRelacion.validarConector(inicio, fin)){
            ComponenteDiagrama conector = new ConectorUMLBasico(inicio, fin, nombre, tipoRelacion);
            return conector;
        }
        return null;
    }
    /**
     * Método para obtener propiedades de la clase
     * <br> Propiedades contenidas:
     * <table>
     * <tr><td>inicio</td><td>->Componente</td></tr>
     * <tr><td>fin</td><td>->Componente</tr>
     * <tr><td>relacion</td><td>->TipoRelacion</tr>
     * </table>
     * @param propiedad nombre de la propiedad que se desea acceder
     * @return La propiedad que se desea obtener
     */
    @Override
    public Object getPropiedad(String propiedad) {
        switch(propiedad){
            case "inicio": return inicio;
            case "fin": return fin;
            case "relacion": return tipoRelacion;
        }
        return null;
    }
    /**
     * Método para ingresar propiedades a la clase
     * <br> Propiedades contenidas:
     * <table>
     * <tr><td>inicio</td><td>->Componente</td></tr>
     * <tr><td>fin</td><td>->Componente</tr>
     * <tr><td>relacion</td><td>->TipoRelacion</tr>
     * </table>
     * @param propiedad nombre de la propiedad a  modificar
     * @param valor valor a insertar en la propiedades
     */
    @Override
    public void setPropiedad(String propiedad, Object valor) {
        switch(propiedad){
            case "inicio": inicio = (Componente) valor; break;
            case "fin": fin = (Componente) valor; break;
            case "relacion": tipoRelacion = (TipoRelacion) valor; break;
        }
    }

    @Override
    public void setFin(ComponenteDiagrama fin) {
        this.fin=fin;
    }

    @Override
    public ComponenteDiagrama getInicio() {
        return inicio;
    }

    @Override
    public void setInicio(ComponenteDiagrama inicio) {
        this.inicio=inicio;
    }

    @Override
    public ComponenteDiagrama getFin() {
        return fin;
    }

}
