/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author roberto
 */
//import Operativo.Conector;
import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.ComponenteSeleccionable;
import Operativo.Conector;
import Operativo.Diagrama;
import Operativo.DirectorConector;
import UML.Caso.FabricaCasoUML;
import contenedorpropiedades.ConstructorContendePropiedad;
import contenedorpropiedades.ContenedorPropiedad;
import contenedorpropiedades.ContenedorPropiedadTextBox;
import contenedorpropiedades.DirectorContenedorPropiedad;
import grafico.Actor;
import grafico.ConstructorFigura;
import grafico.Director;
import grafico.Figura;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class DiagramaUML implements Diagrama, OpcionesUML {

  //protected ArrayList<Conector> conectors;
  private final ArrayList<ComponenteDiagrama> componentes;
  private final FabricaUML fabrica;
  private final DirectorConector director;
  private final JComponent lienzo;
  private ComponenteDiagrama componenteSeleccionado;
  private Director directorGrafico;
  private DirectorContenedorPropiedad dirCP;

    public DiagramaUML(FabricaUML fabrica) {
        this.fabrica = fabrica;
        director = new DirectorConector();
        lienzo = new Figura();
        componentes=new ArrayList<>();
        directorGrafico = new Director();
        directorGrafico.setConstructor(new Actor());
        dirCP=new DirectorContenedorPropiedad();
        dirCP.setConstructor(new ContenedorPropiedadTextBox());
    }

	public DiagramaUML() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

    @Override
    public void actualizarDiagrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Método de creacion de componentes UML
     * <br>Utiliza una fabrica interna para generacion de componentes.
     * <br>Recibe un arreglo de componentes, que debe ir en el siguiente orden.
     * <table border="1">
     * <tr>
     *  <th><i>Atributo</i></th>
     *  <th><i>Tipo</i></th>
     </tr>
     * <tr>
     *  <th>Posicion X</th>
     * <th>int</th>
     * </tr>
     *<tr>
     *  <th>Posicion Y</th>
     *  <th>int</th>
     * </tr>
     * </table>
     * @param parametros Aarreglo de parametros
     * @see FabricaUML
     */
    @Override
    public void crearComponente(Object parametros) {
        ArrayList parametrosCreacion = (ArrayList) parametros;
        ComponenteDiagrama componente = fabrica.generarComponente((String)parametrosCreacion.get(0));
        if(componente != null){
            componente.setConstructor(directorGrafico.getConstructor());
            componentes.add(componente);
            directorGrafico.setParametro("nombre",componente.getNombre());
            directorGrafico.construir();
            componente.setFigura((Figura)directorGrafico.getFigura());

            componente.getFigura().setLocation((int)parametrosCreacion.get(1),(int)parametrosCreacion.get(2));
            lienzo.add(componente.getFigura());
        }
    }

    @Override
    public void seleccionarComponte(int x, int y) {
        componenteSeleccionado=null;
        for(ComponenteDiagrama componente:componentes){
            if(((ComponenteSeleccionable) componente).validarPosicion(x, y)){
                componenteSeleccionado = componente;
                dirCP.setPropiedad(ContenedorPropiedadTextBox.LABELTXT, "TEXTO");
                dirCP.setPropiedad(ContenedorPropiedadTextBox.CAMPOTEXT, componenteSeleccionado.getNombre());
                dirCP.construir();
                break;
            }
        }
    }

    @Override
    public ComponenteDiagrama obtenerSeleccion() {
        return componenteSeleccionado;
    }

    @Override
    public void removerSeleccion() {
        if(componenteSeleccionado!=null){
            componentes.remove(componenteSeleccionado);
			lienzo.remove(componenteSeleccionado.getFigura());
			for(ComponenteDiagrama c : componentes){
				if(c.getClass()==ConectorUMLBasico.class){
					Conector con= (Conector)c;
					if(con.getInicio()==null || con.getFin()==null){
						componentes.remove(con);
						lienzo.remove(con.getFigura());
					}
					if(con.getInicio()==componenteSeleccionado ||con.getFin()==componenteSeleccionado){
						componentes.remove(con);
						lienzo.remove(con.getFigura());
					}
				}
			}
            componenteSeleccionado=null;
        }
    }

    @Override
    public void conectar(ComponenteDiagrama componentes) {
        ComponenteDiagrama conector = director.construirConector(componenteSeleccionado,componentes);
        if(conector!=null)
        {
            this.componentes.add(conector);
            conector.setConstructor(directorGrafico.getConstructor());
            directorGrafico.setParametro("inicioX", componenteSeleccionado.getFigura().getX());
            directorGrafico.setParametro("inicioY", componenteSeleccionado.getFigura().getY());
            directorGrafico.setParametro("finX", componentes.getFigura().getX());
            directorGrafico.setParametro("finY", componentes.getFigura().getY());
            directorGrafico.construir();
            conector.setFigura(directorGrafico.getFigura());
            conector.getFigura().setLocation(componenteSeleccionado.getFigura().getLocation());
            lienzo.add(conector.getFigura());
        }
    }

    @Override
    public void moverSeleccion(int x, int y) {
        if(componenteSeleccionado!= null){
            if(componenteSeleccionado.esMovible()){
                componenteSeleccionado.getFigura().setLocation(x, y);
                for(ComponenteDiagrama c : componentes){
                    if(c.getClass()==ConectorUMLBasico.class){
                        lienzo.remove(c.reconstruirFigura());
                        lienzo.add(c.getFigura());
                    }
                }
            }
        }
    }

    /**
     * @param id
     * @return 
     */
    @Override
    public Object getPropiedadSeleccion(String id) {
        if(componenteSeleccionado!=null)
            return ((Componente)componenteSeleccionado).getPropiedad(id);
        else
            return null;
    }

    @Override
    public void setPropiedadSeleccion(String id, Object valor) {
        if(componenteSeleccionado!=null)
            ((Componente)componenteSeleccionado).setPropiedad(id, valor);
    }

    @Override
    public void setConstructorFigura(ConstructorFigura constructor) {
        directorGrafico.setConstructor(constructor);
    }

    @Override
    public ConstructorFigura getConstructorFigura() {
        return directorGrafico.getConstructor();
    }

    @Override
    public JComponent getLienzo() {
        return lienzo;
    }

    @Override
    public void setPropiedadDiagrama(String id, Object valor) {
        switch (id){
            case "relacion": director.setConstructorConector(
                    fabrica.generarConstructorConector((String)valor)); break;
            default: director.setPropiedad(id, valor); break;
        }
        
    }

    @Override
    public Object getPropiedadDiagrama(String id) {
        return director.getPropiedad(id);
    }

    @Override
    public ComponenteDiagrama obtenerComponente(int x, int y) {
        for(int i=componentes.size()-1; i>=0;i--){
            if(componentes.get(i).validarPosicion(x, y))
                return componentes.get(i);
        }
        return null;
    }

    @Override
    public JComponent getBarraPropiedades() {
        return dirCP.getContenedorPropiedad();
    }

    @Override
    public void notificarCambio(JComponent barraPropiedades) {
        ContenedorPropiedad barra = (ContenedorPropiedad) barraPropiedades;
        JTextField txt = (JTextField) barra.getComponent(1);
		if(componenteSeleccionado!=null){
        componenteSeleccionado.setNombre(txt.getText());
			//System.out.println(txt.getText());
			lienzo.remove(componenteSeleccionado.reconstruirFigura());
			lienzo.add(componenteSeleccionado.getFigura());
		}
    }
}