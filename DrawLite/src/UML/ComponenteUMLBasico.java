/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

import Operativo.Componente;
import Operativo.DecoradorObjeto;
import javax.swing.JComponent;

/**
 * Componente básico para UML
 * @author roberto
 */
public class ComponenteUMLBasico extends DecoradorObjeto {

    private Componente objeto;
    private String tipo;

    public ComponenteUMLBasico(String tipo, Componente objeto) {
        this.objeto=objeto;
        this.tipo= tipo;
    }
    
    public ComponenteUMLBasico(Componente objeto) {
        this.objeto=objeto;
    }

    public ComponenteUMLBasico(){

    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public String getNombre() {
        return objeto.getNombre();
    }

    @Override
    public void setNombre(String name) {
        objeto.setNombre(name);
    }

    @Override
    public boolean esMovible() {
        return true;
    }

    @Override
    public Object getPropiedad(String nombre) {
        switch(nombre){
            case "nombre": return getNombre();
        }
        return null;
    }

    @Override
    public void setPropiedad(String nombre, Object value) {
        switch(nombre){
            case "nombre": setNombre((String) value);
        }
    }

    @Override
    public JComponent reconstruirFigura() {
        JComponent figuraSalida;
        constructor.setParametro("nombre",getNombre());
        constructor.construirFigura();
        figuraSalida=figura;
        figura=constructor.getFigura();
        figura.setLocation(figuraSalida.getLocation());
        return figuraSalida;
    }
    

}