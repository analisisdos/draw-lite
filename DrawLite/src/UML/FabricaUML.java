/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

import Operativo.Componente;
import Operativo.ConstructorConector;

/**
 *
 * @author roberto
 */
public interface FabricaUML {

  
    public Componente generarComponente(String tipo);

    public ConstructorConector generarConstructorConector(String tipo);

}
