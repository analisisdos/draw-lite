/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML.Caso;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import UML.ComponenteUMLBasico;
import UML.TipoRelacion;

/**
 *
 * @author roberto
 */
public class Incluir implements UML.TipoRelacion{

    private static Incluir incluir;

    public boolean validar(Componente start, Componente end) {
        return false;
    }

    public static TipoRelacion crearRelacion() {
        if(incluir == null)
            incluir = new Incluir();
        return incluir;
    }

    private Incluir() {
    }

    @Override
    public String getTipo() {
        return "incluir";
    }

    @Override
    public boolean validarConector(ComponenteDiagrama inicio, ComponenteDiagrama fin) {
        String inicioTipo = ((ComponenteUMLBasico) inicio).getTipo();
        String finTipo = ((ComponenteUMLBasico) fin).getTipo();
        if(finTipo == null || inicioTipo == null)
            return false;
        else if(finTipo.equals("caso") || inicioTipo.equals("caso"))
            return true;
        else
            return false;
    }

}