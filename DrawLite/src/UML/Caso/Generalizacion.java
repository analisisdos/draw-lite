/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML.Caso;

import UML.ComponenteUMLBasico;
import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import UML.TipoRelacion;

/**
 *
 * @author roberto
 */
public class Generalizacion implements TipoRelacion {

    private static Generalizacion generalizacion;

    public boolean validar(Componente start, Componente end) {
        return false;
    }

    public static TipoRelacion crearRelacion() {
        if(generalizacion == null)
            generalizacion = new Generalizacion();
        return generalizacion;
    }

    private Generalizacion() {
    }

    @Override
    public String getTipo() {
        return "generalizacion";
    }

    @Override
    public boolean validarConector(ComponenteDiagrama inicio, ComponenteDiagrama fin) {
        String inicioTipo = ((ComponenteUMLBasico) inicio).getTipo();
        String finTipo = ((ComponenteUMLBasico) fin).getTipo();
        if(finTipo == null || inicioTipo == null)
            return false;
        else if(finTipo.equals("escenario") || inicioTipo.equals("escenario"))
            return false;
        else if(finTipo.equals(inicioTipo))
            return true;
        else
            return false;
    }

}
