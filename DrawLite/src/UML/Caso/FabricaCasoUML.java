/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML.Caso;

import Operativo.Componente;
import Operativo.ConstructorConector;
import Operativo.Objeto;
import UML.ComponenteUMLBasico;
import UML.ConstructorConectorUML;
import UML.FabricaUML;
import grafico.Director;

/**
 *
 * @author roberto
 */
public class FabricaCasoUML implements FabricaUML{

    @Override
    public Componente generarComponente(String tipo) {
        Componente nuevo;
        switch(tipo){
            case "caso": nuevo = new Objeto("Caso"); break;
            case "escenario": nuevo = new Objeto(""); break;
            case "actor" : nuevo = new Objeto("Actor"); break;
            default: return null;
        }
        return new ComponenteUMLBasico(tipo, nuevo);
    }

    @Override
    public ConstructorConector generarConstructorConector(String tipo) {
        ConstructorConector constructor = new ConstructorConectorUML();
        switch(tipo){
            case "generalizacion":
                constructor.setPropiedad("relacion",Generalizacion.crearRelacion());
                break;
            case "incluir":
                constructor.setPropiedad("relacion",Incluir.crearRelacion());
                break;
            case "extender":
                constructor.setPropiedad("relacion",Extender.crearRelacion());
                break;
            case "comunicar":
                constructor.setPropiedad("relacion",Comunicar.crearRelacion());
                break;
        }
        return constructor;
    }
    
}
