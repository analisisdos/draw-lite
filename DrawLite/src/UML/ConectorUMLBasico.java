/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.Conector;
import grafico.Figura;
import javax.swing.JComponent;

/**
 *
 * @author roberto
 */
public class ConectorUMLBasico extends Conector {

    protected String nombre;

    protected TipoRelacion tipoRelacion;

    public ConectorUMLBasico(ComponenteDiagrama inicio, ComponenteDiagrama fin, String nombre, TipoRelacion relacion){
        super(inicio,fin);
        super.setInicio(inicio);
        super.setFin(fin);
        super.setNombre(nombre);
        this.tipoRelacion = relacion;
    }
    @Override
    public void setInicio(ComponenteDiagrama inicio){
        if(tipoRelacion.validarConector(inicio, getFin()))
            super.setInicio(inicio);
    }
    @Override
    public void setFin(ComponenteDiagrama fin){
        if(tipoRelacion.validarConector(getInicio(), fin))
            super.setFin(fin);
    }

     @Override
    public Object getPropiedad(String nombre) {
        switch(nombre){
            case "nombre": return getNombre();
        }
        return null;
    }

    @Override
    public void setPropiedad(String nombre, Object value) {
        switch(nombre){
            case "nombre": setNombre((String) value);
        }
    }

    @Override
    public JComponent reconstruirFigura() {
        JComponent figuraSalida;
        constructor.setParametro("nombre",getNombre());
        constructor.setParametro("inicioX", getInicio().getFigura().getX());
        constructor.setParametro("inicioY", getInicio().getFigura().getY());
        constructor.setParametro("finX", getFin().getFigura().getX());
        constructor.setParametro("finY", getFin().getFigura().getY());
        constructor.construirFigura();
        figuraSalida=figura;
        figura=constructor.getFigura();
        figura.setLocation(getInicio().getFigura().getLocation());
        return figuraSalida;
    }
}