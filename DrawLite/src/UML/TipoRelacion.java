/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;

/**
 *
 * @author roberto
 */
public interface TipoRelacion {
    
  public boolean validarConector(ComponenteDiagrama start, ComponenteDiagrama end);
  
  public String getTipo();

}
