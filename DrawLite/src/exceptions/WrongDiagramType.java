/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Jonathan
 */
public class WrongDiagramType extends Exception{
    public WrongDiagramType(String message){
        super(message);
    }
}
