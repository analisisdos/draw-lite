/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERT;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.ConstructorConectorSimple;
import Operativo.Diagrama;
import Operativo.DirectorConector;
import Operativo.Objeto;
import contenedorpropiedades.DirectorContenedorPropiedad;
import grafico.*;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author SERGIO MALDONADO
 */
public class PERT implements Diagrama{
    
	//Para lo operativo
    private ArrayList<ComponenteDiagrama> objetos;
	private ArrayList<ComponenteDiagrama> conectores;
    private ArrayList<String> rutaCritica;
	
	//Para lo grafico
	Director director;
    private JComponent lienzo;
	private ComponenteDiagrama seleccionado;
	DefaultTableModel tabla;
	
    public PERT(){
        //inicializar
		director = new Director();
        objetos = new ArrayList<>();
		conectores =  new ArrayList<>();
        rutaCritica = new ArrayList<>();
		this.lienzo = new Figura(new Dimension(2000,2000));
		tabla = null;
    }
    /**
	 * Metodo que crea e ingresa una nueva actividad al arreglo del diagrama
	 * @param nombre Nombre de la actividad
	 * @param tiempos Tiempos (pesimista,probable,optimizta)
	 */
    public void ingresarActividad(String nombre,int[]tiempos){
        Componente actividad = new Objeto(nombre);
        actividad = new Actividad(actividad,tiempos);
        objetos.add(actividad);
    }
    /**
     * Metodo que agrega una nueva transicion al diagrama
     * @param act1 La actividad seleccionada
     * @param act2 La actividad que precede a la actividad seleccionada
     */
    
    public void ingresarTransicion(String act1,String act2){
        int i,j;
        // se obtiene el primer id de la lista que es la actividad actual
        for(i = 0;i<objetos.size();i++){
            if(((Actividad)objetos.get(i)).getNombre().equals(act1)){
               break; 
            }
        }
        //se procede a calcular precedencia y sucerosa
        //((Actividad)objetos.get(i)).ingresarSucesora((Objeto)act2);
        ((Actividad)objetos.get(i)).ingresarPredecesora(encontrarActi(act2));
    }
	
	/**
	 * Metodo que encuentra una actividad
	 * @param nomb Nombre de la actividad
	 * @return retorna el objeto Actividad
	 */
    public Actividad encontrarActi(String nomb){
        Actividad aux = null;
        for (int i = 0;i<objetos.size();i++){
            if(((Actividad)objetos.get(i)).getNombre().equals(nomb)){
                aux = ((Actividad)objetos.get(i));
                return aux;
            }
        }
        return null;
    }
	/**
	 * Metodo que encuentra una actividad
	 * @param nomb Nombre de la actividad
	 * @return retorna la posicion de la actividad
	 */
    public int encontrarActividad(String nomb){
        int i = 0;
        for (i = 0;i<objetos.size();i++){
            if(((Actividad)objetos.get(i)).getNombre().equals(nomb)){
                break;
            }
        }
        return i;
    }
    
    /**
     * Compara si las predecesoras de la actividad ya se les ha calculado su inicio y fin cercano
     * 
     * @param act
     * @return retorna 0 si no se ha calculado, retorna 1 si ya se calcularon 
     */
    public int compararPrede(Actividad act){
        ArrayList<ComponenteDiagrama> aux = act.predecesoras;
        for(int i =0;i<aux.size();i++){
            if((((Actividad)aux.get(i)).getInicioFin(0)==0)&&(((Actividad)aux.get(i)).getInicioFin(1)==0)){
                return 0;
            }
        }
        return 1;
    }
    
    public int compararSuce(Actividad act){
        ArrayList<ComponenteDiagrama> aux = act.sucesoras;
        for(int i =0;i<aux.size();i++){
            if((((Actividad)aux.get(i)).getInicioFin(2)==0)&&(((Actividad)aux.get(i)).getInicioFin(3)==0)){
                return 0;
            }
        }
        return 1;
    }
    
    public void haciaAdelante(){
        boolean bandera = true;
        while(bandera==true){
            bandera = false;
            for(int i = 0;i<objetos.size();i++){
                if(((Actividad)objetos.get(i)).predecesoras.size() == 0){
                    //System.out.println("if");
                    ((Actividad)objetos.get(i)).setInicioFin(0, 0);
                    ((Actividad)objetos.get(i)).setInicioFin(((Actividad)objetos.get(i)).getTiempos()[1],1);
                    ((Actividad)objetos.get(i)).setInicioFin(0,2);
                    ((Actividad)objetos.get(i)).setInicioFin(((Actividad)objetos.get(i)).getTiempos()[1],3);
                }
                else if (compararPrede((Actividad)objetos.get(i))==1){ //podemos calcular holguras
                    //System.out.println("else if");
                    ((Actividad)objetos.get(i)).calcularCercano();

                }
                else{
                    if(compararPrede(((Actividad)objetos.get(i)))==1){//podemos calcular holguras
                        //System.out.println("else if");
                        ((Actividad)objetos.get(i)).calcularCercano();
                    }
                    else{
                        //System.out.println("else");
                        //((Actividad)objetos.get(i)).calcularCercano();
                        bandera = true;
                    }
                }
                //((Actividad)objetos.get(i)).calcularCercano();
            }
        }
        
    }
    
    public void haciaAtras(){
        boolean bandera = true;
        while(bandera==true){
            bandera = false;
            for(int i = objetos.size()-1;i>=0;i--){
                if(((Actividad)objetos.get(i)).sucesoras.size() == 0){
                    ((Actividad)objetos.get(i)).setInicioFin(((Actividad)objetos.get(i)).getInicioFin(0), 2);
                    ((Actividad)objetos.get(i)).setInicioFin(((Actividad)objetos.get(i)).getInicioFin(1),3);
                }
                else{
                    if (compararSuce((Actividad)objetos.get(i))==1){ //podemos calcular holguras
                        ((Actividad)objetos.get(i)).calcularLejano();
                    }
                    else{
                        //((Actividad)objetos.get(i)).calcularCercano();
                        bandera = true;
                    }
                }
            }
        }
    }
    public void calcularHolguras(){
        haciaAdelante();
        haciaAtras();
        for(int i = 0; i <objetos.size();i++){
            ((Actividad)objetos.get(i)).calcularHolgura();
			//System.out.println("i: " + i);
			this.tabla.setValueAt(((Actividad)objetos.get(i)).getHolgura(), i, 5);
        }
    }
    public void calcularRutaCritica(){
        for(int i = 0;i<objetos.size();i++){
            if(((Actividad)objetos.get(i)).getHolgura() == 0){
                rutaCritica.add(((Actividad)objetos.get(i)).getNombre());
            } 
        }
    }

    public ArrayList<String> getRutaCritica() {
        return rutaCritica;
    }
    
    
    
    public void calcularSucesora(String nombre){
        int act = encontrarActividad(nombre);
        ComponenteDiagrama aux2 = null;
        for (int i = 0; i< objetos.size();i++){
            for (int j = 0; j<((Actividad)objetos.get(i)).predecesoras.size();j++){
                aux2= ((Actividad)objetos.get(i)).predecesoras.get(j);
                if(((Actividad)objetos.get(i)).getNombre().equals(aux2.getNombre())){
                //if(((Objeto)objetos.get(i)).getNombre().equals(((Objeto)aux2).getNombre())){
                    ((Actividad)objetos.get(act)).ingresarSucesora(aux2);
                }
            }
        }
    }
    public void calcularSucesoras(){
        Actividad aux = null; // actividad posterior que evaluaremos
        Actividad auxi = null;
        for(int i = 0; i<objetos.size();i++){
            if(i == objetos.size()-1){
                
            }
            else{
                for(int j = i+1;j<objetos.size();j++){
                    aux = ((Actividad)objetos.get(j));
                    for(int k = 0;k<aux.predecesoras.size();k++){
                        auxi = ((Actividad)aux.predecesoras.get(k));
                        //auxi = ((Actividad)((Actividad)objetos.get(j)).predecesoras.get(k));
                        if(((Actividad)objetos.get(i)).getNombre().equals(auxi.getNombre())){
                        //if(((Objeto)objetos.get(i)).getNombre().equals(aux.getNombre())){
                            ((Actividad)objetos.get(i)).ingresarSucesora(aux);
                        }
                    }
                }
            }
        }
    }
    
    public void mostrarConsola(){
        for(int i = 0; i<objetos.size();i++){
            System.out.println("------------------------------------------------");
            ((Actividad)objetos.get(i)).mostrarConsola();
        }
    }
	/**
	 * Metodo que construye toda la red a partir de la informacion de un Jtable
	 * Lee toda la informacion de Jtable, la cual debe tener las columnas:
	 *			Actividad
	 *			Predecesoras (Si ha varias deben ir separadas por una coma ",")
	 *			Tiempo Pesimista
	 *			Tiempo Probable
	 *			Tiempo Optimizta
	 *			Holgura (No editable)
	 * @param tabla El modelo de la tabla
	 */
    public void construirDesdeTabla(DefaultTableModel tabla){
		this.tabla = tabla;
		String prede = "";
		String [] predec;
		for(int fil = 0;fil<tabla.getRowCount();fil++){
			ingresarActividad((String)tabla.getValueAt(fil, 0), new int[]{Integer.parseInt(((String)(tabla.getValueAt(fil, 2)))),
																		  Integer.parseInt(((String)(tabla.getValueAt(fil, 3)))),
																		  Integer.parseInt(((String)(tabla.getValueAt(fil, 4))))});

			prede = (String)tabla.getValueAt(fil, 1);
			if(prede.equals("")||prede.equals("-")){
				//no hacer nada jajjaaja
			}
			else{
				prede = prede.replaceAll(" ", "");
				predec = prede.split(",");
				for(int i = 0;i<predec.length;i++){
					ingresarTransicion((String)tabla.getValueAt(fil,0), predec[i]);
				}
			}
		}
	}
	private ArrayList<ComponenteDiagrama> evaluarPrede(ArrayList<ComponenteDiagrama> acti){
		ArrayList<ComponenteDiagrama> activi = new ArrayList<ComponenteDiagrama>();
		ArrayList<ComponenteDiagrama> aux;
		for(int i = 0;i<acti.size();i++){
			aux = ((Actividad)acti.get(i)).predecesoras;
			for(int j=0;j<aux.size();j++){
				activi.add(aux.get(j));
			}
		}
		return activi;
		
	}
	private int obtenerCapa(ArrayList<ComponenteDiagrama> acti){
		if(acti.size()==0){
			return 1;
		}
		else{
			return 1 + obtenerCapa(evaluarPrede(acti));
		}
	}
	/**
	 * Metodo que llama a los metodos graficarContenedores y graficarConectores
	 */
	public void graficar(){
		graficarContenedores();
		graficarConectores();
	}
	
	/**
	 * Metodo que grafica todas las actividades con su respectivo objeto grafico
	 * RectanguloPert
	 * Este metodo hace uso del metodo de la misma clase obtenerCapa
	 *		Es usado para calcular el numero de capa al que pertenece cada actividad
	 */
	public void graficarContenedores(){

		director.setConstructor(new RectanguloPert());
		String nom = "";
		//Se van a crear primero las actividades
		JComponent acti;
		ArrayList<ComponenteDiagrama> aux;
		String[] datos;
		ArrayList<int[]> capas = new ArrayList<>();
		int numeroCapa = -1;
		
		//Empieza la graficacion de actividades
		//Se setean los datos de la actividad
		for(int i = 0; i< objetos.size(); i++){
			if(((Actividad)objetos.get(i)).getHolgura()==0)
				nom = (String)objetos.get(i).getNombre() + "*";
			else
				nom = (String)objetos.get(i).getNombre();
			datos = new String[]{objetos.get(i).getPropiedad("inicioC").toString(),nom,objetos.get(i).getPropiedad("inicioL").toString()
								,objetos.get(i).getPropiedad("finC").toString(),objetos.get(i).getPropiedad("tiempo2").toString(),objetos.get(i).getPropiedad("finL").toString()};
			director.setParametro("texto", datos);
			director.construir();
			acti = director.getFigura();
			aux = new ArrayList<ComponenteDiagrama>();
			aux = (((Actividad)objetos.get(i)).predecesoras);
			numeroCapa = obtenerCapa(aux) -1 ; //Para iniciar en 0 se le va restar uno
			//System.out.println("NUMERO CAPA " + numeroCapa);
			/*
				Se realizan los calculos de las posiciones para la actividad
				Para ello se ha dividio en capas
				Donde la numeracion de las capas va de 1 an
				Se hace uso de la funcion obtenerCapa, la cual regresa el numero de la capa que pertenece
					Si la capa ya existe entonces la actividad se va a agregar en esa capa (posX) con 50 pixeles en y abajo de la ultima actividad
					Si la capa no existe se crea y se empieza en un y=50 y un x calculable en 70 pixeles a la derecha de la capa anterior
					Si no existe ni una sola capa (Actividad sin precedencias) el punto inicial es 50,50
			*/
			if(capas.size()==0){
//				//System.out.println("if");
				capas.add(new int[]{20,20});//punto inicial
			}
			else{
				//System.out.println("else");
				//verificamos a que capa pertenece
				if(numeroCapa+1<=capas.size()){//significa que la capa ya existe
					//System.out.println("\tif");
					//el valor de la capa en x seguira siendo el mismo
					//el valor en y se trasladara 50 para abajo
					capas.set(numeroCapa, new int[]{capas.get(numeroCapa)[0],capas.get(numeroCapa)[1]+60}); //editamos los valores en la capa seleccionada
				}
				else{//capa nueva
					//System.out.println("\telse");
					//El -1 para indicar una capa anterior
					//ya que la numeracion de las capas es de 1 en adelante
					capas.add(new int[]{capas.get(numeroCapa-1)[0]+200,capas.get(0)[1]});//punto inicial nueva capa
				}
			}
			//System.out.println(capas.get(numeroCapa)[0] + " " + capas.get(numeroCapa)[1]);
			acti.setLocation(capas.get(numeroCapa)[0], capas.get(numeroCapa)[1]);
			this.lienzo.add(acti);
			objetos.get(i).setFigura(acti);
			//falta agregarlo a componentes o arreglo de figuras
			
		}
	}
	/**
	 * Metodo que grafica las flechas de transicion entre actividades
	 * Grafica a traves de sus sucesoras
	 */
	public void graficarConectores(){
		ArrayList<ComponenteDiagrama> o2 = new ArrayList<ComponenteDiagrama>();
		director.setConstructor(new FlechaPert());
		director.setParametro("tipo", Triangulo.DERECHA);
		director.setParametro("llena", true);
		for (ComponenteDiagrama objeto : objetos) {
			Dimension d = objeto.getFigura().getSize();
			o2 = ((Actividad)objeto).sucesoras;
			director.setParametro("nombre", "");
			director.setParametro("inicioX", (int)d.getWidth() + objeto.getFigura().getX());
			director.setParametro("inicioY", (int)d.getHeight()/2 + objeto.getFigura().getY());
			for (ComponenteDiagrama aux : o2) {
				d = aux.getFigura().getSize();
				director.setParametro("finX", aux.getFigura().getX());
				director.setParametro("finY", aux.getFigura().getY()+ (int)d.getHeight()/2);
				director.construir();
				JComponent fig = director.getFigura();
				this.lienzo.add(fig);
			}
		}
	}
	
	@Override
	public void actualizarDiagrama() {
		this.lienzo = new Figura(new Dimension(2000,2000));
		graficar();
	}

	/**
	 * Construye el diagrama completo
	 * @param nombre No implementado, puede ser null
	 * @param parametros Un objecto DefaultTableModel
	 */
	@Override
	public void crearComponente(Object parametros) {
		this.tabla = (DefaultTableModel)parametros;
		construirDesdeTabla(tabla);
		calcularSucesoras();
		calcularHolguras();
		mostrarConsola();
		calcularRutaCritica();
	}

	@Override
	public void seleccionarComponte(int x, int y) {
		for (ComponenteDiagrama objeto : objetos) {
			if(objeto.validarPosicion(x, y)){
				this.seleccionado = objeto;
			}
		}
	}

	@Override
	public ComponenteDiagrama obtenerSeleccion() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void removerSeleccion() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void conectar(ComponenteDiagrama componentes) {
		DirectorConector bobConector = new DirectorConector(new ConstructorConectorSimple());
		ComponenteDiagrama nuevo = bobConector.construirConector(seleccionado, componentes);
		
		//se le asigna el caracter
		director.setParametro("nombre", objetos);
		//lo construye y asigna a todo
		director.construir();
		nuevo.setFigura(director.getFigura());
		this.objetos.add(nuevo);
		this.lienzo.add(director.getFigura());
		
		
	}

	@Override
	public void moverSeleccion(int x, int y) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object getPropiedadSeleccion(String id) {
		return  null;
	}

	@Override
	public void setPropiedadSeleccion(String id, Object valor) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setConstructorFigura(ConstructorFigura constructor) {
		director.setConstructor(constructor);
	}

	@Override
	public ConstructorFigura getConstructorFigura() {
		return director.getConstructor();
	}
	
	@Override
	public JComponent getLienzo(){
		return this.lienzo;
	}

	@Override
	public void setPropiedadDiagrama(String id, Object valor) {
		if(id.equals("tabla"))
			this.tabla = (DefaultTableModel)valor;
	}

	@Override
	public Object getPropiedadDiagrama(String id) {
		if(id.equals("ruta"))
			return rutaCritica;
		if(id.equals("tabla"))
			return this.tabla;
		else
			return null;
	}

	@Override
	public ComponenteDiagrama obtenerComponente(int x, int y) {
		for (ComponenteDiagrama objeto : objetos) {
			if(objeto.validarPosicion(x, y)){
				return objeto;
			}
		}
		return null;
	}

	@Override
	public JComponent getBarraPropiedades() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void notificarCambio(JComponent barraPropiedades) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

   
}
