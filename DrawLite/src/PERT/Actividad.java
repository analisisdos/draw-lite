/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERT;

import Operativo.*;
import Operativo.DecoradorObjeto;
import Operativo.Objeto;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author SERGIO MALDONADO
 */
public class Actividad extends DecoradorObjeto{
	public ComponenteDiagrama actividad;
    private int tiempos [];
    private int holgura;
    public ArrayList<ComponenteDiagrama> predecesoras;
    public ArrayList<ComponenteDiagrama> sucesoras;
    private int [] inicioFin; // inicio Cercano fin cercano inicio lejano fin lejano
    
    
    public Actividad(ComponenteDiagrama ob,int[] tiempos) {
        this.actividad = ob;
        this.tiempos = tiempos;
        this.inicioFin = new int[4];
        this.inicioFin[0] = 0;
        this.inicioFin[1] = 0;
        this.inicioFin[2] = 0;
        this.inicioFin[3] = 0;
        this.predecesoras = new ArrayList<ComponenteDiagrama>();
        this.sucesoras = new ArrayList<ComponenteDiagrama>();
    }
    public void ingresarPredecesora(ComponenteDiagrama actividad){
        predecesoras.add(actividad);
    }
    public void ingresarSucesora(ComponenteDiagrama actividad){
        sucesoras.add(actividad);
    }

    public void setHolgura(int holgura) {
        this.holgura = holgura;
    }
    public int getInicioFin(int pos)
    {
        return inicioFin[pos];
    }
    public void setInicioFin(int dato, int pos) {
        this.inicioFin[pos] = dato;
    }
    
    public int[] getTiempos() {
        return tiempos;
    }

    public int getHolgura() {
        return holgura;
    }
    
    public void calcularHolgura(){
        this.holgura = (this.inicioFin[0]) - (this.inicioFin[2]);
    }
    
    public void calcularCercano(){
        int aux = 0;
        for (int i = 0; i< predecesoras.size();i++){
            if ((((Actividad)predecesoras.get(i)).inicioFin[1]) > aux){
                aux = (((Actividad)predecesoras.get(i)).inicioFin[1]);
            }
        }
        this.inicioFin[0] = aux;
        this.inicioFin[1] = (this.tiempos[1]) + aux;
    }
    
    public void calcularLejano(){
        int aux = 50000;
        for (int i = 0; i< sucesoras.size();i++){
            if ((((Actividad)sucesoras.get(i)).inicioFin[2]) < aux){
                aux = (((Actividad)sucesoras.get(i)).inicioFin[2]);
            }
        }
        this.inicioFin[3] = aux;
        this.inicioFin[2] = aux - (this.tiempos[1]);
    }
    public void mostrarConsola(){
        System.out.println("Actividad: " + actividad.getNombre());
        System.out.println("\tTiempo m: " + this.tiempos[1]);
        System.out.println("Actividades Predecesoras:");
        for(int i = 0;i<predecesoras.size();i++){
            System.out.println("\tActividad: " + ((Actividad)predecesoras.get(i)).getNombre());
        }
        System.out.println("Actividades Sucesoras:");
        for(int i = 0;i<sucesoras.size();i++){
            System.out.println("\tActividad: " + ((Actividad)sucesoras.get(i)).getNombre());
        }
        System.out.println("InicioC: " + inicioFin[0] + " FinC: " + inicioFin[1]);
        System.out.println("InicioL: " + inicioFin[2] + " FinL: " + inicioFin[3]);
        System.out.println("Holgura: " + this.holgura);
    }

	@Override
	public String getNombre() {
		return actividad.getNombre();
	}

	@Override
	public void setNombre(String nombre) {
		actividad.setNombre(nombre);
	}


    @Override
    public boolean esMovible() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public Object getPropiedad(String nombre) {
		if (nombre.equals("tiempo1")) //Tiempo pesimista
			return this.tiempos[0];
		if (nombre.equals("tiempo2")) //Tiempo probable
			return this.tiempos[1];
		if (nombre.equals("tiempo3")) //Tiempo optimizta
			return this.tiempos[2];
		if (nombre.equals("inicioC")) //inicioC	
			return this.inicioFin[0];
		if (nombre.equals("inicioL")) //inicioL
			return this.inicioFin[1];
		if (nombre.equals("finC"))    //finC
			return this.inicioFin[2];
		if (nombre.equals("finL"))    //finL
			return this.inicioFin[3];
		else
			return null;
	}

	/**
	 * 
	 * @param nombre
	 * @param value 
	 */
	@Override
	public void setPropiedad(String nombre, Object value) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public JComponent reconstruirFigura() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
