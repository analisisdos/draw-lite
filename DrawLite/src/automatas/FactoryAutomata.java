/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas;

import java.util.ArrayList;

/**
 *
 * @author SERGIO MALDONADO
 */
public interface FactoryAutomata {
    public Automata crearAutomata(int tipo,ArrayList<String> alfabeto);
}
