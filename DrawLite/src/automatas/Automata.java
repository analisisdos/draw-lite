/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas;

import Operativo.Diagrama;

/**
 *
 * @author SERGIO MALDONADO
 */
public interface Automata extends Diagrama{
    public int ingresarEstado(String nombre,int tipo);
	public int ingresarTransicion(String nombre,String estado, String caracter);
    public int ingresarTransiciones(String nombre,String[] estados, String[] caracteres);
    public int borrarTransicion(String nombre, String caracter);
    public void validarCadena(String cadena);
	public void setParametro(String tipo,Object parametro);
	public Object getParametro(String tipo);
}
