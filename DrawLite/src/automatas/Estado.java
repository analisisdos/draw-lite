/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas;

import Operativo.Componente;
import Operativo.DecoradorObjeto;
import Operativo.Objeto;
import grafico.CirculoDoble;
import grafico.CirculoDobleInicial;
import grafico.CirculoInicial;
import grafico.CirculoSimple;
import grafico.ConstructorFigura;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author SERGIO MALDONADO
 */
public class Estado extends DecoradorObjeto{
    public Componente estado;
    private boolean esInicial;
    private boolean esFinal;
    private ArrayList<String[]> transiciones; // estado,caracter

    public Estado(Componente est,boolean esInicial, boolean esFinal) {
        this.estado = est;
        this.esInicial = esInicial;
        this.esFinal = esFinal;
        this.transiciones = new ArrayList();
    }
    
    public int crearEstado(String[] estados, String[] caracteres){
        
        for(int i = 0; i < estados.length;i++){
            if (crearTransicion(estados[i],caracteres[i])==0){
                return 0;
            }
        }
        return 1;
        
    }
    
    public int crearTransicion(String estado, String caracter){
        boolean aceptar = true;
		/*
        for (int i = 0;i<transiciones.size();i++){
            if(caracter.equals(transiciones.get(i)[1])){
                //indica que ya hay una transicion con el mismo caracter
                aceptar = false;
            }
        }
*/
        if (aceptar==true){
            //si se puede ingresar
            transiciones.add(new String[]{estado,caracter});
            return 1;
        }
        else{
            //no se puede
            return 0;
        }
    }
    public void mostrarConsola(){
        System.out.println("Estado: " + ((Objeto)estado).getNombre());
        System.out.println("Inicial: " + this.esInicial);
        System.out.println("Final: " + this.esFinal);
        System.out.println("Transiciones: ");
        for(int i = 0; i<transiciones.size();i++){
            System.out.println("\tCaracter: " + transiciones.get(i)[1] + " hacia Estado: " + transiciones.get(i)[0]);
        }
    }

	@Override
	public void setNombre(String nombre) {
		estado.setNombre(nombre);
	}

    @Override
    public boolean esMovible() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public Object getPropiedad(String nombre) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setPropiedad(String nombre, Object value) {
		if(nombre.equals("nombre"))
			this.estado.setNombre(value.toString());
		if(nombre.equals("inicial"))
			this.esInicial = (boolean)value;
		if(nombre.equals("final"))
			this.esFinal = (boolean)value;
		if(nombre.equals("figura"))
			super.setFigura((JComponent)value);
	}

	@Override
	public String getNombre() {
		return estado.getNombre();
	}

	@Override
	public JComponent reconstruirFigura() {
		JComponent figuraAnterior;
		if(this.esInicial&&this.esFinal){
			constructor = new CirculoDobleInicial();
		}
		else if((this.esInicial)&&(!this.esFinal)){
			constructor = new CirculoInicial();
		}
		else if((!this.esInicial)&&(this.esFinal)){
			constructor = new CirculoDoble();
		}
		else if((!this.esInicial)&&(!this.esFinal)){
			constructor = new CirculoSimple();
		}
		constructor.setParametro("nombre", this.getNombre());
		constructor.construirFigura();
		figuraAnterior = figura;
		figura = constructor.getFigura();
		figura.setLocation(figuraAnterior.getLocation());
		return figuraAnterior;
	}

}
