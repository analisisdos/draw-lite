/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatas;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.Conector;
import Operativo.ConectorSimple;
import Operativo.ConstructorConectorSimple;
import Operativo.DirectorConector;
import Operativo.Objeto;
import contenedorpropiedades.*;
import grafico.ConstructorFigura;
import grafico.Director;
import grafico.Figura;
import grafico.Triangulo;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author SERGIO MALDONADO
 */
public class AFD implements Automata{
	
	//Para lo operativo
    private ArrayList<ComponenteDiagrama> objetos;
    private ArrayList<String> alfabeto;
    private String expresionRegular;
    private boolean yaInicial;
	
	//Para lo grafico
	Director director;
	private ComponenteDiagrama seleccionado;
    private JComponent lienzo;
	private int numeroEstado;
	
	//Para la edicion
	DirectorContenedorPropiedad directorPropiedad;
	//auxiliares
	private String caracter;
	
    public AFD(ArrayList alpha){
        this.alfabeto = alpha;
        objetos = new ArrayList<>();
        this.yaInicial = false;
		director = new Director();
		this.lienzo = new Figura(new Dimension(2000,2000));
		this.caracter = null;
		this.numeroEstado = 0;
		directorPropiedad = new DirectorContenedorPropiedad();
    }
    
	public AFD(){
		objetos = new ArrayList<>();
        this.yaInicial = false;
		director = new Director();
		this.lienzo = new Figura(new Dimension(2000,2000));
		this.caracter = null;
		this.numeroEstado = 0;
	}
	
    public void mostrarConsola(){
        System.out.println("Automata");
        for(int i = 0; i < objetos.size();i++){
            ((Estado)objetos.get(i)).mostrarConsola();
        }
    }
    
    public void generarExp(){
        
    }
    
    public int encontrarEstado(String nombre){
        for(int i =0; i< objetos.size();i++){
			if(objetos.get(i).getNombre().equals(nombre)){
            //if(((Estado)objetos.get(i)).getNombre().equals(nombre)){
                return i;
            }
        }
        return -1;
    }
    
    @Override
    public int ingresarEstado(String nombre, int tipo) {
        //Object a = new ArrayList<>();
		boolean inicial = false;
        boolean finaL = false;
        
        if(tipo == 1){ // es inicial pero no final
            inicial = true;
            finaL = false;
            if(yaInicial==false){
                yaInicial = true;
            }
            else{
                return 0 ;
            }
        }
        if(tipo == 2){ // es inicial y final
            inicial = true;
            finaL = true;
            if(yaInicial==false){
                yaInicial = true;
            }
            else{
                return 0 ;
            }
        }
        if(tipo == 3){ // no es inicial ni final
            inicial = false;
            finaL = false;
        }
        if(tipo == 4){ // no es inicial pro si final
            inicial = false;
            finaL = true;
        }
        
        Componente objeto = new Objeto(nombre);
        objeto = new Estado(objeto,inicial,finaL);
        objetos.add(objeto);
        return 1;
    }
	/**
	 * Metodo que compara si el caracter para una transicion es parte del alfabeto
	 * @param caracter Caracter a comparar
	 * @return true si es parte del alfabeto, false si no es parte
	 */
	public boolean caracterAceptado(String caracter){
		for (int i = 0; i < alfabeto.size(); i++){
			if(caracter == alfabeto.get(i))
				return true;
		}
		return false;
	}
	
    @Override
    public int ingresarTransiciones(String nombre,String[] estados,String[] caracteres) {
        int pos = encontrarEstado(nombre);
        return ((Estado)(objetos.get(pos))).crearEstado(estados, caracteres);
    }

    @Override
    public int borrarTransicion(String nombre, String caracter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validarCadena(String cadena) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public void actualizarDiagrama() {
		this.lienzo.revalidate();
		this.lienzo.repaint();
	}

	@Override
	public void crearComponente(Object parametros) {
		ArrayList parm = (ArrayList)parametros;
		//int tipo = Integer.parseInt(parm.get(1));
		int tipo = (int)parm.get(1);
		int res = ingresarEstado((String)parm.get(0), tipo);
		director.setParametro("nombre", parm.get(0));
		director.construir();
		JComponent figura = director.getFigura();
		if(res ==1){
			objetos.get(objetos.size()-1).setFigura(figura);
			figura.setLocation((int)parm.get(2), (int)parm.get(3));
			//figura.setLocation(Integer.parseInt(parm.get(2)+""),Integer.parseInt(parm.get(3)+""));
			this.lienzo.add(figura);
		}
		
	}

	@Override
	public void seleccionarComponte(int x, int y) {
		this.seleccionado = null;
		for (ComponenteDiagrama objeto : objetos) {
			if(objeto.validarPosicion(x, y)){
				this.seleccionado = objeto;
				if(seleccionado.getClass() == ConectorSimple.class){
					directorPropiedad.setConstructor(new ContenedorPropiedadTextBox());
					directorPropiedad.setPropiedad(ContenedorPropiedadTextBox.LABELTXT, "Nombre");
					directorPropiedad.setPropiedad(ContenedorPropiedadTextBox.CAMPOTEXT, seleccionado.getNombre());
				}
				if(seleccionado.getClass()== Estado.class){
					directorPropiedad.setConstructor(new ContenedorRadioButon());
					directorPropiedad.setPropiedad(ContenedorRadioButon.LABELTXT, "Nombre");
					directorPropiedad.setPropiedad(ContenedorRadioButon.CAMPOTEXT, seleccionado.getNombre());
					directorPropiedad.setPropiedad(ContenedorRadioButon.LABELRDB, "Tipo");
					directorPropiedad.setPropiedad(ContenedorRadioButon.RADIO1, "Inicial");
					directorPropiedad.setPropiedad(ContenedorRadioButon.RADIO2, "Final");
				}
				directorPropiedad.construir();
				
			}
				
		}
	}

	@Override
	public ComponenteDiagrama obtenerSeleccion() {
		return this.seleccionado;
	}

	@Override
	public void removerSeleccion() {
		if(seleccionado!=null){
            objetos.remove(seleccionado);
			lienzo.remove(seleccionado.getFigura());
			for(ComponenteDiagrama c : objetos){
				if(c.getClass()==ConectorSimple.class){
					Conector con= (Conector)c;
					if(con.getInicio()==null || con.getFin()==null){
						objetos.remove(con);
						lienzo.remove(con.getFigura());
					}
					if(con.getInicio()==seleccionado ||con.getFin()==seleccionado){
						objetos.remove(con);
						lienzo.remove(con.getFigura());
					}
				}
			}
            seleccionado=null;
        }
	}
	private int tipoConector(int inicioX, int inicioY, int finX, int finY){
		if((inicioX<finX)&&(inicioY<finY))
			return 1;
		else if((inicioX<finX)&&(inicioY>finY))
			return 2;
		else if((inicioX>finX)&&(inicioY<finY))
			return 3;
		else if((inicioX>finX)&&(inicioY>finY))
			return 4;
		else
			return 1;
	}
	
	private int tipoTriangulo(int inicioX, int inicioY, int finX, int finY){
		if((inicioY<finY)&&(inicioX==finX))
			return Triangulo.ABAJO;
		else if((inicioY>finY)&&(inicioX==finX))
			return Triangulo.ARRIBA;
		else if(inicioX<finX)
			return Triangulo.DERECHA;
		else if(inicioX>finX)
			return Triangulo.IZQUIERDA;
		else
			return Triangulo.DERECHA;
	}
	/**
	 * Metodo general que crea una transicion
	 * @param componentes Componente destino
	 */
	@Override
	public void conectar(ComponenteDiagrama componentes) {
		//implementar la conexion
		DirectorConector bobConector = new DirectorConector(new ConstructorConectorSimple());
		ComponenteDiagrama nuevo = bobConector.construirConector(seleccionado, componentes);
		
		//se le asigna el caracter
		//el director ya tiene su builder
		director.setParametro("nombre", this.caracter);
		//le asigno el inicio y fin
		director.setParametro("llena", true);
		JComponent arista = null;
		//lo construye y asigna a todo
		//Verifica segun las coordenadas la direccion del triangulo
		int resp = tipoConector(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(),
								seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2,
								componentes.getFigura().getX(),
								componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
		
		//Se le indica su setlocation dependiendo de las coordenadas
		System.out.println("set location es tipo:" + resp);
		
		System.out.println("X1: " + seleccionado.getFigura().getX() + "Y1: " + seleccionado.getFigura().getY());
		System.out.println("X2: " + componentes.getFigura().getX() + "Y2: " + componentes.getFigura().getY());
		if(resp == 1){
			director.setParametro("inicioX", seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX());
			director.setParametro("inicioY", seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("finX", componentes.getFigura().getX());
			director.setParametro("finY", componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("tipo", tipoTriangulo(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(),
													seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2,
													componentes.getFigura().getX(),
													componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2));
			director.construir();
			arista = director.getFigura();
			arista.setLocation(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(), seleccionado.getFigura().getY() + seleccionado.getFigura().getWidth()/2);
		}
		else if(resp == 2){
			director.setParametro("inicioX", seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX());
			director.setParametro("inicioY", seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("finX", componentes.getFigura().getX());
			director.setParametro("finY", componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("tipo", tipoTriangulo(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(),
													seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2,
													componentes.getFigura().getX(),
													componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2));
			director.construir();
			arista = director.getFigura();
			arista.setLocation(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(), componentes.getFigura().getY()+componentes.getFigura().getHeight()/2);
		}
		else if(resp ==3){
			director.setParametro("inicioX", seleccionado.getFigura().getX());
			director.setParametro("inicioY", seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("finX", componentes.getFigura().getX()+componentes.getFigura().getWidth());
			director.setParametro("finY", componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("tipo", tipoTriangulo(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(),
													seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2,
													componentes.getFigura().getX(),
													componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2));
			director.construir();
			arista = director.getFigura();
			arista.setLocation(componentes.getFigura().getWidth()+componentes.getFigura().getX(), seleccionado.getFigura().getY()+seleccionado.getFigura().getHeight()/2);
		}
		else if(resp == 4){
			director.setParametro("inicioX", seleccionado.getFigura().getX());
			director.setParametro("inicioY", seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("finX", componentes.getFigura().getX()+componentes.getFigura().getWidth());
			director.setParametro("finY", componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2);
			director.setParametro("tipo", tipoTriangulo(seleccionado.getFigura().getWidth()+seleccionado.getFigura().getX(),
													seleccionado.getFigura().getY() + seleccionado.getFigura().getHeight()/2,
													componentes.getFigura().getX(),
													componentes.getFigura().getY() + seleccionado.getFigura().getHeight()/2));
			director.construir();
			arista = director.getFigura();
			arista.setLocation(componentes.getFigura().getWidth()+componentes.getFigura().getX(), componentes.getFigura().getY()+componentes.getFigura().getHeight()/2);
		}
		//Se almacena la figura en todo
		nuevo.setFigura(arista);
		this.objetos.add(nuevo);
		this.lienzo.add(arista);
		
		ingresarTransicion(seleccionado.getNombre(), componentes.getNombre(), this.caracter);
		this.seleccionado = null;
	}

	@Override
	public void moverSeleccion(int x, int y) {
		
	}

	@Override
	public Object getPropiedadSeleccion(String id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setPropiedadSeleccion(String id, Object valor) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setParametro(String tipo, Object parametro) {
		if(tipo.equals("alfabeto"))
			this.alfabeto = (ArrayList<String>)parametro;
		if (tipo.equals("caracter"))
			this.caracter = (String)parametro;
		if(tipo.equals("nuevaTransicion")){
			ArrayList<Object> parm = (ArrayList<Object>)parametro;
			ingresarTransicion(((Estado)this.seleccionado).getNombre(), ((Estado)parm.get(0)).getNombre(), parm.get(1).toString());
			conectar((ComponenteDiagrama)parm.get(0));
		}
		//if(tipo.equals("estadoActual"))
			//this.estadoActual = (String)parametro;
	}

	@Override
	public Object getParametro(String tipo) {
		if(tipo.equals("alfabeto"))
			return alfabeto;
		else
			return null;
	}
	
	/**
	 * Metodo para ingresar una transicion tanto en lo operativo como grafico 
	 * @param nombre Estado origen
	 * @param estado Estado destino
	 * @param caracter Caracter para la transicion
	 * @return -1 si el caracter no pertenece al alfabeto, 0 si no se pudo realizar y 1 si se pudo realizar
	 */
	@Override
	public int ingresarTransicion(String nombre, String estado, String caracter) {
		int pos = encontrarEstado(nombre);
		//if(caracterAceptado(caracter)){// es parte del alfabeto
			return ((Estado)(objetos.get(pos))).crearTransicion(estado, caracter);
		//}
        //return -1;
	}

	@Override
	public void setConstructorFigura(ConstructorFigura constructor) {
		director.setConstructor(constructor);
	}

	@Override
	public ConstructorFigura getConstructorFigura() {
		return director.getConstructor();
	}
	
	@Override
	public JComponent getLienzo(){
		return this.lienzo;
	}

	@Override
	public void setPropiedadDiagrama(String id, Object valor) {
		if(id.equals("alfabeto"))
			this.alfabeto = (ArrayList<String>)valor;
		if (id.equals("caracter"))
			this.caracter = (String)valor;
		if(id.equals("nuevaTransicion")){
			ArrayList<Object> parm = (ArrayList<Object>)valor;
			ingresarTransicion(((Estado)this.seleccionado).getNombre(), ((Estado)parm.get(0)).getNombre(), parm.get(1).toString());
			conectar((ComponenteDiagrama)parm.get(0));
		}
		if(id.equals("tipo")){
			
		}
	}

	@Override
	public Object getPropiedadDiagrama(String id) {
		if(id.equals("numeroEstado")){
			this.numeroEstado++;
			return this.numeroEstado;
		}
		else
			return null;
	}

	@Override
	public ComponenteDiagrama obtenerComponente(int x, int y) {
		for (ComponenteDiagrama objeto : objetos) {
			if(objeto.validarPosicion(x, y)){
				return objeto;
			}
		}
		return null;
	}

	@Override
	public JComponent getBarraPropiedades() {
		return directorPropiedad.getContenedorPropiedad();
	}

	@Override
	public void notificarCambio(JComponent barraPropiedades) {
		ContenedorPropiedad ob = (ContenedorPropiedad)barraPropiedades;
		JTextField text = (JTextField)ob.getComponent(1);//nombre
		if(seleccionado.getClass() == ConectorSimple.class){
			seleccionado.setNombre(text.getText());
			
		}
		if(seleccionado.getClass() == Estado.class){
			JRadioButton r1 = (JRadioButton)ob.getComponent(4);//inicial
			JRadioButton r2 = (JRadioButton)ob.getComponent(5);//final

			seleccionado.setNombre(text.getText());
			System.out.println(text.getText());
			seleccionado.setPropiedad("inicial", r1.isSelected());
						System.out.println(r1.isSelected());
			seleccionado.setPropiedad("final", r2.isSelected());
						System.out.println(r2.isSelected());

			
		}
		JComponent viejo = seleccionado.reconstruirFigura();
		this.lienzo.remove(viejo);
		this.lienzo.add(seleccionado.getFigura());
		
	}
}
