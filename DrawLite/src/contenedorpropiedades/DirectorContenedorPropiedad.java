/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contenedorpropiedades;

import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public class DirectorContenedorPropiedad {
    private ConstructorContendePropiedad constructor;
    public DirectorContenedorPropiedad() {
        
    }

    public void setConstructor(ConstructorContendePropiedad constructor) {
        this.constructor = constructor;
    }
    
    public void setPropiedad(int id, Object propiedad){
        this.constructor.setPropiedad(id, propiedad);
    }
    
    public void construir(){
        this.constructor.construir();
    }
    
    public JComponent getContenedorPropiedad(){
        return this.constructor.getContenedorPropiedad();
    }
	
	public Object getPropiedad(int id){
		return this.constructor.getPropiedad(id);
	}
    
}
