/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contenedorpropiedades;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author RM
 */
public class ContenedorPropiedadTextBox implements ConstructorContendePropiedad{
    private JLabel label;
    private JTextField campo;
    public static final int LABELTXT = 0;
    public static final int CAMPOTEXT = 1;
    private JComponent contenedor;
    private int alto;
    @Override
    
    
    
    public void setPropiedad(int id, Object propiedad) {
        switch(id){
            case ContenedorPropiedadTextBox.LABELTXT: label = new JLabel((String) propiedad);
            break;
            case ContenedorPropiedadTextBox.CAMPOTEXT: campo = new JTextField((String) propiedad);
            break;
            
        }
    }

    public ContenedorPropiedadTextBox() {
        this.alto = 50;
    }

    @Override
    public void construir() {
        int ancho = label.getText().length() * 8;
        
        label.setSize(new Dimension(ancho + 4, alto +1 ));
        ancho = campo.getText().length() * 8;
        
        campo.setSize(new Dimension(ancho + 4, alto +1 ));
        
        contenedor = new ContenedorPropiedad(new Dimension(200, alto - 15));
        
        contenedor.setLayout(new GridLayout(1, 2));
        
        contenedor.add(label);
        contenedor.add(campo);
        
        
        
    }

    @Override
    public JComponent getContenedorPropiedad() {
        return this.contenedor;
    }

	@Override
	public Object getPropiedad(int id) {
		switch(id){
            case ContenedorPropiedadTextBox.CAMPOTEXT :return campo.getText();
        }
		return null;
	}
    
}
