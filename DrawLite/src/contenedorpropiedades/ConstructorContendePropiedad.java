/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contenedorpropiedades;

import javax.swing.JComponent;

/**
 *
 * @author RM
 */
public interface ConstructorContendePropiedad {
    public void setPropiedad(int id, Object propiedad);
    public void construir();
    public JComponent getContenedorPropiedad();
	public Object getPropiedad(int id);
}
