/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contenedorpropiedades;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author RM
 */
public class ContenedorRadioButon implements ConstructorContendePropiedad{
    private JComponent contenedor;
    private JRadioButton radioButon1, radioButon2;
    private JTextField campo;
    private JLabel etiquetaTXT, etiquetaRDB;
    public static final int LABELTXT = 0;
    public static final int LABELRDB = 3;
    public static final int RADIO1 = 1;
    public static final int RADIO2 = 2;
    public static final int CAMPOTEXT = 4;
	private int alto;
	
	public ContenedorRadioButon() {
		this.alto = 50;
	}
    
    
    @Override
    public void setPropiedad(int id, Object propiedad) {
        switch(id){
			
            case ContenedorRadioButon.LABELTXT: etiquetaTXT = new JLabel((String) propiedad);
            break;
            case ContenedorRadioButon.LABELRDB: etiquetaRDB = new JLabel((String) propiedad);
            break;
            case ContenedorRadioButon.RADIO1: radioButon1 = new JRadioButton((String) propiedad);
			break;
            case ContenedorRadioButon.RADIO2: radioButon2 = new JRadioButton((String) propiedad);
			break;
            case ContenedorRadioButon.CAMPOTEXT: campo = new JTextField((String) propiedad);
            break;
         
        }
    }

    @Override
    public void construir() {
        int ancho = etiquetaTXT.getText().length() * 8;
        
        etiquetaTXT.setSize(new Dimension(ancho + 4, alto +1 ));
		
		ancho = radioButon1.getText().length() *8;
		
		radioButon1.setSize(new Dimension(ancho + 4, alto +1 ));
		
		ancho = radioButon2.getText().length() *8;
		
		radioButon2.setSize(new Dimension(ancho + 4, alto +1 ));
		
		ancho = etiquetaRDB.getText().length() * 8;
        
        etiquetaRDB.setSize(new Dimension(ancho + 4, alto +1 ));
        ancho = campo.getText().length() * 8;
        
        campo.setSize(new Dimension(ancho + 4, alto +1 ));
		
		contenedor = new ContenedorPropiedad(new Dimension(300, alto));
		
		contenedor.setLayout(new GridLayout(2, 3));
		
		contenedor.add(etiquetaTXT);
		contenedor.add(campo);
		contenedor.add(new JComponent() {
});
		contenedor.add(etiquetaRDB);
		contenedor.add(radioButon1);
		contenedor.add(radioButon2);
		
    }

    @Override
    public JComponent getContenedorPropiedad() {
        return this.contenedor;
    }

	@Override
	public Object getPropiedad(int id) {
		switch(id){
            case ContenedorRadioButon.RADIO1: return radioButon1.isSelected();
            case ContenedorRadioButon.RADIO2: return radioButon2.isSelected();
        }
		return null;
	}
    
}
