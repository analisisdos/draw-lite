/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RNA;

import Operativo.Componente;
import Operativo.ComponenteDiagrama;
import Operativo.ConstructorConectorSimple;
import Operativo.Diagrama;
import Operativo.DirectorConector;
import grafico.ConstructorFigura;
import grafico.Director;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author RM
 */
public class RNA implements Diagrama{
    ArrayList<Capa> capas;
    ArrayList<ComponenteDiagrama> componentes;
    ComponenteDiagrama seleccionado;
    Director director;

    public RNA() {
        this.capas = new ArrayList<>();
        this.componentes = new ArrayList<>();
        this.seleccionado = null;
        this.director = new Director();
    }
    
    public void nuevaCapa(Capa capa){
       this.capas.add(capa);
    }
    
    public void removerCapa(Capa capa){
        this.capas.remove(capa);
    }
    
    public void getCapa(int capa){
        this.capas.get(capa);
    }
    
    @Override
    public void actualizarDiagrama() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void crearComponente(Object parametros) {
        
    }

    @Override
    public void seleccionarComponte(int x, int y) {
        for (ComponenteDiagrama componente: componentes) {
            if (componente.validarPosicion(x, y)) {
                seleccionado = componente;
            }
        }
    }

    @Override
    public ComponenteDiagrama obtenerSeleccion() {
        return this.seleccionado;
    }

    @Override
    public void removerSeleccion() {
        this.seleccionado = null;
    }

    @Override
    public void moverSeleccion(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getPropiedadSeleccion(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPropiedadSeleccion(String id, Object valor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConstructorFigura(ConstructorFigura constructor) {
        this.director.setConstructor(constructor);
    }

    @Override
    public ConstructorFigura getConstructorFigura() {
       return this.director.getConstructor();
    }

    @Override
    public void conectar(ComponenteDiagrama componente) {
        DirectorConector bobConector = new DirectorConector(new ConstructorConectorSimple());
        componentes.add(bobConector.construirConector(seleccionado, componente));
    }

	@Override
	public JComponent getLienzo() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setPropiedadDiagrama(String id, Object valor) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Object getPropiedadDiagrama(String id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public ComponenteDiagrama obtenerComponente(int x, int y) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public JComponent getBarraPropiedades() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void notificarCambio(JComponent barraPropiedades) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
    
}
